#ifndef _FILTER_H

#include "GProcessing.h"

#define STEPS_BEFORE_MC		0
#define STEPS_JUMP_AT_M		2
#define HISTORY_LEN		10
#define GC1(PFIL) 		((2.0 * (2.0 * PFIL - 1.0)) / (PFIL * (PFIL + 1.0)))
#define GC2(PFIL) 		(6.0 / (PFIL * (PFIL + 1.0)))

/*
 * Maneuver check strobe.
 * TODO: read from config file
 */

#define MCP_A_TIA	0.5
#define MCP_E_TIA	0.5
#define MCP_D_TIA	100
#define MCP_A_TWS	0.5
#define MCP_E_TWS	0.5
#define MCP_D_TWS	100
#define DEFAULT_K	4.0

using namespace slib;
using namespace geo;

class Filter
{
public:
	enum Types {
		FT_KALMAN,
		FT_UNDEFINED,
	};

	struct MData {
		CoordSphere		PosSph;
		CoordDec3D		PosDec;
		Velocity		V;
	};

	struct IData {
		unsigned		id;
		struct timespec		LocTime;
		CoordGeo		SrcPosGeo;
		double			dtime;
		struct MData		pinput;
		struct MData		poutput;
		double			P[3];
		struct IData*		next;
		struct IData*		prev;
	};

protected:
		enum Types		type;
		bool			inited;
		unsigned		nsteps;
		unsigned		steps;
		size_t			history_len;
		struct timespec		tstart;
		struct timespec		tlast;
		struct IData		*history;
		struct IData		*current;
		CoordSphere		MP[TM_MAX + 1];
		double			K[TM_MAX + 1];

protected:
		unsigned	CheckManeuver(CoordSphere &A, CoordSphere &B, int mode);
		int	DoKalman(unsigned mode);

public:

			Filter();
			~Filter();

	bool		isInited(void);
	unsigned	getNumSteps(void);
	struct timespec &getWorkTime(struct timespec &);
	void		Reset(void);
	void		Tune(struct GDataSource::SourceType &st);
	struct GTrackPoint &processPoint(struct GTrackPoint &arg, TrackingMode mode);
};

#endif
