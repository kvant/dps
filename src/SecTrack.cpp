#include <string.h>
#include "SecProcessing.h"
#include "GObjects.h"

#define HASH	0xB00B5

#define DEFAULT_DROP_INTERVAL 20

#define MAX_MISS_COUNT_CAP 1
#define MAX_MISS_COUNT_TWS 1
#define MAX_MISS_COUNT_TIA 3

#ifndef MODEL_INPUT
#define MIN_TIA_INTERVAL 0.3
#else
#define MIN_TIA_INTERVAL 5.0
#endif

#define DMP_DEFAULT (DMP_SRC_BLH | DMP_TRG_XYZ | DMP_V_XYZ |  \
			DMP_VR | DMP_COURSE | DMP_PARAMETER | \
			DMP_ATIME | DMP_MODE | DMP_APPROACH | \
			DMP_DANGER | DMP_H | DMP_TCOURSE |    \
			DMP_OT | DMP_MANEUVER)

enum StrobeVar {
	STR_D,
	STR_A,
	STR_E,
	STR_T
};

using namespace slib::timesupport;

char* gtrpnt2str(struct GTrackPoint& _dest, int flags)
{
	static char str[800]={0};
	if (!flags)
		return str;
	sprintf(str, "T(%04lu:%04lu)MN(%d);", _dest.LocTime.tv_sec % 1000,
			_dest.LocTime.tv_nsec / 1000000, _dest.ModId);

	if (flags & DMP_SRC_BLH)
		sprintf((str + strlen(str)), " sgc(%.2f, %.2f, %.2f)",
			_dest.SrcPosGeo.B, _dest.SrcPosGeo.L, _dest.SrcPosGeo.H);

	if (flags & DMP_TRG_XYZ)
		sprintf((str + strlen(str)), " tdc( %.2f %.2f %.2f )",
				_dest.Pos.X, _dest.Pos.Y, _dest.Pos.Z);

	if (flags & DMP_V_XYZ)
		sprintf((str + strlen(str)), " tvc(%03.02f, %03.02f, %03.02f):%03.02f",
			_dest.V.Vx, _dest.V.Vy, _dest.V.Vz, _dest.V.Vabs);

	if (flags & DMP_VR)
		sprintf((str + strlen(str)), " tvr(%03.02f)", _dest.Vr);

	if (flags & DMP_COURSE)
		sprintf((str + strlen(str)), " C(%03.02f)", RAD_TO_DEG(_dest.Course));

	if (flags & DMP_PARAMETER)
		sprintf((str + strlen(str)), " P(%05.02f)", _dest.Parameter);

	if (flags & DMP_H)
		sprintf((str + strlen(str)), " H(%05.02f)", _dest.H);

	if (flags & DMP_TCOURSE)
		sprintf((str + strlen(str)), " TC(%03.02f)", RAD_TO_DEG(_dest.TCourse));

	if (flags & DMP_ATIME)
		sprintf((str + strlen(str)), " AT(%03.02f)", _dest.ATime);

	if (flags & DMP_DANGER)
		sprintf((str + strlen(str)), " DNG(%d)", _dest.Danger);
	if (flags & DMP_MANEUVER)
		sprintf((str + strlen(str)), " MD(%s%s%s)",
				_dest.Maneuver & BIT(0) ? "D":"_",
				_dest.Maneuver & BIT(1) ? "A":"_",
				_dest.Maneuver & BIT(2) ? "E":"_");

	if (flags & DMP_APPROACH)
		sprintf((str + strlen(str)), " APR(%s)", _dest.Approaching ?
				"YES" : "NO");

	if (flags & DMP_MODE)
		sprintf((str + strlen(str)), " M(%s)",
			(_dest.Mode == TM_CAPTURE) ? "CAP" :
			(_dest.Mode == TM_TWS) ?  "TWS" : "TIA");

	if (flags & DMP_RMS_XYZ)
		sprintf((str + strlen(str))," rc(%.2f, %.2f, %.2f)",
			_dest.CrdRMS.RMSx, _dest.CrdRMS.RMSy, _dest.CrdRMS.RMSz);

	if (flags & DMP_RMS_V)
		sprintf((str+strlen(str)), " rv(%.2f, %.2f, %.2f)",
			_dest.VelRMS.RMSx, _dest.VelRMS.RMSy, _dest.VelRMS.RMSz);


	if (flags & DMP_OT) sprintf((str+strlen(str)), " OT(%s)",
			((_dest.ObjType == LO_TYPE_UNCKNOWN) ? "UN" :
			((_dest.ObjType == LO_TYPE_AIRCRAFT) ? "AIR" :
			((_dest.ObjType == LO_TYPE_MISSILE) ? "MSL" :
			((_dest.ObjType == LO_TYPE_MARINE) ? "MAR" :
			((_dest.ObjType == LO_TYPE_JAMMER)?"JAM":"?"))))));

	if (flags & DMP_ORC) sprintf((str+strlen(str)), " ORC(%s)",
			((_dest.ORC == LO_NF_UNKNOWN) ? "UN":
			((_dest.ORC == LO_NF_ENEMY) ? "EN" :
			((_dest.ORC == LO_NF_FRIENDLY) ? "FR" : "FRbr"))));

	return str;
}

SecTrack::SecTrack (uint32_t id, SecDataSource* src, Strobe *str, void* pr_data):
	SelfId(id),
	SelfType(TT_SECONDARY),
	TStrobe(str),
	Updated(false),
	Obsolete(false),
	Source(src),
	Log(slib::log::SLog::Instance().CreateSession("", 0, NULL))
{
	char name[256];
	sprintf(name,"log/sources/src_%d/tracks/tr_%04d.log", src->GetId(), id);
	SLog::Instance().AliasSession(Log, name);
	Log.SetName("Tr");
	Log << info << "Track_" << (int)id << " created in source_"
			<< (int)src->GetId()<<endr;
	GObjects.GTimeSystem->GetCurrentTime(LastPasTime);
	HistoryPeriod.tv_sec = 240;
	HistoryPeriod.tv_nsec = 0;
	NumUpdates = 0;
	AvInterval = 1e1;
	Mode = TM_CAPTURE;
	MissCount = 0;
	TEupdate = 0;
	SFilter.Tune(Source->GetSelfType());
	MMask = Source->GetSelfType().ModeMask;
}

SecTrack::~SecTrack ()
{
	LOGD(Log, ">>>>> Track killed  <<<<");
}

void SecTrack::SetUpdated(bool enable)
{
	MissCount = 0;
	Updated = enable;
}

void SecTrack::SetMarker(uint32_t marker)
{
	Marker = marker;
}

void SecTrack::DropMarker(void)
{
	Marker = 0;
}

uint32_t SecTrack::GetModId(void)
{
	return Points.back().ModId;
}

uint32_t SecTrack::GetSelfId(void)
{
	return SelfId;
}

uint32_t SecTrack::GetSrcId(void)
{
	return Source->GetId();
}

uint32_t SecTrack::GetMarker(void)
{
	return Marker;
}

bool SecTrack::GetUpdated(void)
{
	return Updated;
}

void SecTrack::UpdateTargetType(GTrackPoint &p)
{
/*
	if (Mode == TM_CAPTURE) {
		p.ObjType = LO_TYPE_UNCKNOWN;
		return;
	}
*/
	if (p.H <= TC_V_LE) {
		if (p.V.Vabs <= TC_V_LE) {
			if (p.PosSph.Distance <= TC_D_LE)
				p.ObjType = LO_TYPE_HELICOPTER;
			else
				p.ObjType = LO_TYPE_MARINE;
		} else if (p.PosSph.Distance <= TC_D_ME) {
			if (p.Parameter <= TC_P_LE)
				p.ObjType = LO_TYPE_MISSILE;
			else
				p.ObjType = LO_TYPE_AIRCRAFT;
		} else {
				p.ObjType = LO_TYPE_AIRCRAFT;
		}
	} else {
		if (p.H <= TC_H_ME)
			if (p.V.Vabs <= TC_V_ME) {
				p.ObjType = LO_TYPE_HELICOPTER;
				return;
			}
		if (p.V.Vabs <= TC_V_HE) {
			if (p.PosSph.Distance >= TC_D_HE) {
				p.ObjType = LO_TYPE_AIRCRAFT;
			} else {
				if (p.Parameter <= TC_P_LE)
					p.ObjType = LO_TYPE_MISSILE;
				else
					p.ObjType = LO_TYPE_AIRCRAFT;
			}
		} else {
			p.ObjType = LO_TYPE_MISSILE;
		}
	}
}

void SecTrack::UpdateDangerous(GTrackPoint &p)
{
#if 0
	p.Danger = (((p.TCourse <= (M_PI / 4.0)) &&
			(p.Vr < 0) &&
			(p.Parameter <= 8000.0)) ||
			(Mode == TM_CAPTURE)) ? true : false;
#else
	p.Danger = DANGER_LEVEL_3;

	if (p.PosSph.Distance <= DANGEROUS_D_RANGE &&
			p.H <= DANGEROUS_H_RANGE &&
			p.Vr >= DANGEROUS_V1_RANGE) {
		p.Danger = DANGER_LEVEL_1;
	} else if (p.Vr >= DANGEROUS_V2_RANGE &&
			p.Parameter <= DANGEROUS_P_RANGE) {
		p.Danger = DANGER_LEVEL_2;
	}
#endif
}

void SecTrack::UpdateTrackingMode(GTrackPoint &p)
{
	if (Points.size() < TM_CAPTURE_TH)
		Mode = p.Mode = TM_CAPTURE;
	else
		Mode = p.Mode = ((MMask & (BIT(TM_TIA) || BIT(TM_TWS))) == (BIT(TM_TIA) || BIT(TM_TWS))) ?
			(p.Danger ? TM_TIA : TM_TWS) : (MMask & BIT(TM_TIA) ? TM_TIA : TM_TWS);
}

void SecTrack::RequestForUpdate(void)
{
	SetTTU();

	if (Mode != TM_TWS)
		Source->RequestForUpdate(this);

	SetupTE(BIT(Mode));
}

void SecTrack::UpdateTrackingTime(GTrackPoint &p)
{
	UPdt = (p.Mode == TM_TWS) ? DEFAULT_DROP_INTERVAL :
		TStrobe[p.Mode].GetInterval(p.PosSph.Distance).time;
}

void SecTrack::CompleteTrackData(GTrackPoint &p)
{
	/* -course
	 * -parameter
	 * -atime
	 * -approaching
	 */
	double b;
	double t1, t2;

	p.Course = GetCourseByV(p.V);
	p.Vr = GetVRadial(p.Pos, p.V);
	p.Parameter = GetCourseParameter(p.Pos, p.Course);
	p.H = p.Pos.Z + (p.Pos.X * p.Pos.X + p.Pos.Y * p.Pos.Y + p.Pos.Z * p.Pos.Z) /
		(2 * 6378245) + 40;

	p.TCourse = acos(-p.Vr / p.V.Vabs);
#if 0
	b = p.V.Vx * p.V.Vx + p.V.Vy * p.V.Vy + p.V.Vz * p.V.Vz;
	t1 = (p.V.Vx * p.Pos.X + p.V.Vy * p.Pos.Y + p.V.Vz * p.Pos.Z) / b;
	t2 = (sqrt(p.Pos.X * p.Pos.X + p.Pos.Y * p.Pos.Y + p.Pos.Z * p.Pos.Z) *
			fabs(sin(p.TCourse))) / sqrt(b);
	p.ATime = t1 + t2;
#endif
	p.ATime = GetATime(p.Pos, p.V);
	p.Approaching = IsApproaching(p.Pos, p.Course);
}

inline void SecTrack::SetupTE(unsigned types)
{
	struct  TimeSystem::timeEvent te = {
		DBL2TSPEC_INIT(UPdt),
		te_type:TE_MISS_TIA,
		te_arg1:Source,
		te_arg2:this,
		SecDataSource::TimeEventProxy
	};

	GetTTU(&te.te_time);
	TEupdate = GObjects.GTimeSystem->InsertEvent(te);
	LOGD(Log, "Set TO: " << UPdt << " Ev #" << (int)TEupdate);
}

inline void SecTrack::DropTE(unsigned types)
{
	GObjects.GTimeSystem->RemoveEvent(TEupdate);
}

void inline SecTrack::SendLast(void)
{
	Source->PutMessage(0, MT_OUT_TP, &Points.back(),
				sizeof(struct GTrackPoint));
}

GTrackPoint& SecTrack::InsertNewPoint(GScanData& point, int vn)
{
	static GTrackPoint pnt;
	GTrackPoint npoint = {0};

	DropTE(0xFF);

	npoint.Id = SelfId;
#ifdef MODEL_INPUT
	npoint.ModId = (point.Task > 100000) ?
		point.Task - 100000 : point.Task;

#elif defined(MINTERACTIVE)
	npoint.ModId = point.TaskM;
#endif
	npoint.LocTime = point.LocTime;
	npoint.PosSph.Azimuth = point.Azimuth;
	npoint.PosSph.Elevation = point.Elevation;
	npoint.PosSph.Distance = point.Var[vn].Distance;
	npoint.Pos = point.Var[vn].PosDec;
	npoint.SrcPosGeo = point.SrcPosGeo;
	npoint.ORC = point.Var[vn].ORC;
	npoint.Vr = point.Var[vn].Vr;

	SFilter.processPoint(npoint, Mode);

	CompleteTrackData(npoint);

	UpdateTargetType(npoint);
	UpdateDangerous(npoint);
	UpdateTrackingMode(npoint);
	UpdateTrackingTime(npoint);

	Points.push_back(npoint);

	RequestForUpdate();


	SetMarker(PM_BINDED | PM_PROCESSED);
	GSDMarkSV(point, vn, PM_BINDED | PM_PROCESSED);

	Log << info << gtrpnt2str(npoint, DMP_DEFAULT) << endr;

	LastTime   = Points.back().LocTime;
	OldestTime = Points.front().LocTime;

	while((LastTime - OldestTime) > HistoryPeriod) {
		Points.erase(Points.begin());
		OldestTime = Points.front().LocTime;
	}

	SetUpdated(true);
	SendLast();
	return Points.back();
}

int SecTrack::GetLastPoint(GTrackPoint& point, uint32_t flags)
{
	if (Points.empty())
		return -1;
	point = Points.back();
	return 0;
}

int SecTrack::GetHistory(vector<GTrackPoint>& history, uint32_t flags)
{
	history = Points;
	return 0;
}

int SecTrack::GetPointByND(struct GTrackPoint& point, struct NaviDataMsg& nd)
{
	return 0;
}

int SecTrack::IsObsolete (void)
{
	return 0;
}

int SecTrack::RecalcPoint(struct GTrackPoint& _pnt)
{
	return 0;
}

int SecTrack::ExtrapolatePoint(struct GTrackPoint& pnt, struct NaviDataMsg& nd, unsigned fl)
{
	CMatrix_t matrix;
	CoordDec3D res_c;
	struct timespec dts = nd.Time - pnt.LocTime;
	double dt = TSPEC2DBL(dts);
	double k = (1e1 + dt) / 1e1;

	if (fl & PFL_EXTR_NO_RMS) {
		memset(&pnt.CrdRMS, 0x00, sizeof(pnt.CrdRMS));
	} else {
		pnt.CrdRMS.RMSx *= k;
		pnt.CrdRMS.RMSy *= k;
		pnt.CrdRMS.RMSz *= k;
		pnt.CrdRMS.RMSxy = sqrt(pnt.CrdRMS.RMSx * pnt.CrdRMS.RMSx +
					pnt.CrdRMS.RMSy * pnt.CrdRMS.RMSy);
		pnt.CrdRMS.RMSxyz = sqrt(pnt.CrdRMS.RMSx * pnt.CrdRMS.RMSx +
					 pnt.CrdRMS.RMSy * pnt.CrdRMS.RMSy +
					 pnt.CrdRMS.RMSz * pnt.CrdRMS.RMSz);
	}

	CalcCMatrix(pnt.SrcPosGeo, nd.Geo, matrix);
	ApplyMatrix(pnt.Pos, matrix);

	pnt.Pos.X += pnt.V.Vx * dt;
	pnt.Pos.Y += pnt.V.Vy * dt;
	pnt.Pos.Z += pnt.V.Vz * dt;
	Dec3D2Sphere(pnt.Pos, pnt.PosSph);
	pnt.SrcPosGeo = nd.Geo;
	pnt.LocTime = nd.Time;

	return 0;
}

GTrackPoint& SecTrack::GetLastPoint (uint32_t flags)
{
	static GTrackPoint point;
	if (Points.empty())
		return point;
	return Points.back();
}

GTrackPoint &SecTrack::GetExtrapolated(struct NaviDataMsg& nd)
{
	/* Sanity */
	LETPoint = GetLastPoint();
	if (Points.size() == 1)
		goto skip;
	if (nd.Time < LETPoint.LocTime) {
		LOGW(Log, "Warning: Extrapolation to past !!!");
	}
	LEdt = TSPEC2DBL((nd.Time - LETPoint.LocTime));

	ExtrapolatePoint(LETPoint, nd);
skip:
	return LETPoint;
}

GTrackPoint& SecTrack::Finalize( struct NaviDataMsg& nd, uint32_t reason )
{
	Points.push_back(GetExtrapolated(nd));
	Points.back().Feature = slib::proto::MSG_F_TRACK_DROPED;
	SendLast();
	return Points.back();
}

void SecTrack::GetLastTime    (struct timepascal& _lt)
{
	_lt = LastPasTime;
}

bool SecTrack::InStrobe(struct GScanData &sd, unsigned flags, uint8_t vn, double *d)
{
	StrobeCResult str_res;
	TrackingMode str_mode;
	Strobe::SCMode str_type;
	double dd, da, de;
	unsigned strr = 0;
	struct NaviDataMsg nd = {
		Time: sd.LocTime,
		Geo: sd.SrcPosGeo,
		0,
	};
/* TODO: Check and confirm */
//#ifdef MODEL_INPUT
	if(sd.LocTime == LastTime) {
		strr |= BIT(STR_T);
		goto miss;
	}
//#endif

	if(nd.Time != LETPoint.LocTime)
		GetExtrapolated(nd);

	switch(Mode) {
	case TM_CAPTURE:
		if (sd.Task != SelfId) { /* should be strobed only in TIA mode */
			goto miss;
		}
		str_mode = TM_CAPTURE;
		str_type = Strobe::SCM_LAZY;
		break;
	case TM_TWS:
		if (sd.Task != SelfId) { /* seems ok */
			str_mode = TM_TWS;
			str_type = Strobe::SCM_FULL;
		} else { /* why we here ?! lets do the same */
			str_mode = TM_TWS;
			str_type = Strobe::SCM_FULL;
		}
		break;
	case TM_TIA:
		if (sd.Task == SelfId) {
			str_mode = TM_TIA;
			str_type = Strobe::SCM_LAZY;
		} else {
			str_mode = TM_TWS;
			str_type = Strobe::SCM_FULL;
		}
		break;
	}

	TStrobe[str_mode].Calculate(LETPoint.PosSph, LEdt, str_type, str_res);

	if (flags & STR_DISTANCE) {
		dd = fabs(GetDistance(LETPoint.Pos) - GetDistance(sd.Var[vn].PosDec));
		if (dd > str_res.sph.Distance) {
			strr |= BIT(STR_D);
			goto miss;
		}
	}

	if (str_type != Strobe::SCM_LAZY) {
		if (flags & STR_AZIMUTH) {
			da = DeltaAngleRad(LETPoint.PosSph.Azimuth, sd.Azimuth);
			if (da > str_res.sph.Azimuth) {
				strr |= BIT(STR_A);
				goto miss;
			}
		}
		if (flags & STR_ELEVATION) {
			de = DeltaAngleRad(LETPoint.PosSph.Elevation, sd.Elevation);
			if (de > str_res.sph.Elevation) {
				strr |= BIT(STR_E);
				goto miss;
			}
		}
	}

	if (d)
		*d = GetDistance(LETPoint.Pos, sd.Var[vn].PosDec);

	LOGD(Log, "Strobe(_OK_) tsk#" << (int)sd.Task << " R:" << \
			  (strr & BIT(STR_D) ? "D" : "_") << \
			  (strr & BIT(STR_A) ? "A" : "_") << \
			  (strr & BIT(STR_E) ? "E" : "_") << \
			  (strr & BIT(STR_T) ? "T" : "_") << \
			"  D:" << dd << "/" << str_res.sph.Distance << \
			"  A:" << RAD_TO_DEG(da) << "/" << RAD_TO_DEG(str_res.sph.Azimuth) << \
			"  E:" << RAD_TO_DEG(de) << "/" << RAD_TO_DEG(str_res.sph.Elevation) );

	return true;
miss:
	LOGD(Log, "Strobe(MISS) tsk#" << (int)sd.Task <<
			" R:" <<
			  (strr & BIT(STR_D) ? "D" : "_") <<
			  (strr & BIT(STR_A) ? "A" : "_") <<
			  (strr & BIT(STR_E) ? "E" : "_") <<
			  (strr & BIT(STR_T) ? "T" : "_") <<
			"  D:" << dd << "/" << str_res.sph.Distance <<
			"  A:" << RAD_TO_DEG(da) << "/" << RAD_TO_DEG(str_res.sph.Azimuth) << \
			"  E:" << RAD_TO_DEG(de) << "/" << RAD_TO_DEG(str_res.sph.Elevation) );

	if (d)
		*d = 10e10;

	return false;
}

bool SecTrack::QuickUpdate(struct GScanData &sd, unsigned flags, int *vn)
{
	bool updated = false;
	double range, mrange = 10e10;
	unsigned mnv = HASH;

	if (!sd.NVarP)
		return false;

	for (unsigned nv = 0; nv < sd.NVar; nv++) {
		if (InStrobe(sd, flags, nv, &range)) {
			if (range < mrange) {
				mrange = range;
				mnv = nv;
			}
		}
	}

	if (mnv ^ HASH) {
		InsertNewPoint(sd, mnv);
		updated = true;
		if (vn)
			*vn = mnv;
	} else {
		/* Don't process this track again */
		SetMarker(PM_BINDED | PM_PROCESSED);
	}

	return updated;
}

bool SecTrack::Miss(void)
{
	bool res = false;

	switch (Mode) {
#ifndef MODEL_INPUT
	case TM_CAPTURE:
#endif
	case TM_TWS:
		/* Drop after first miss */
		res = true;
		break;
#ifdef MODEL_INPUT
	case TM_CAPTURE:
#endif
	case TM_TIA:
		/* Switch to min interval and request again */
		if (++MissCount == MAX_MISS_COUNT_TIA) {
			res = true;
		} else {
			UPdt = MIN_TIA_INTERVAL;
			RequestForUpdate();
		}
		break;
	}

	LOGW(Log, "Miss happens. Mode = " << (Mode == TM_CAPTURE ? "CAP" :
			(Mode == TM_TWS ? "TWS":"TIA")) << " count = " << (int)MissCount
			<< " decision = " << (res ? "KILL" : "REREQUEST") << " Id ="
			<< (int)SelfId);

	return res;
}

void SecTrack::GetTTU(struct timespec *ttu)
{
	*ttu = TTU;
}

inline void SecTrack::SetTTU(void)
{
	GObjects.GTimeSystem->GetCurrentTime(TTU);
	TTU += UPdt;
}

