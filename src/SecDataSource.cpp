#include <string.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <errno.h>
#include "GObjects.h"
#include "SecProcessing.h"

#define _MIN(a,b) (((a) < (b)) ? (a) : (b))
#define _MAX(a,b) (((a) > (b)) ? (a) : (b))

#define NEW_SAMS_NUM(chan, new_num)	(chan * 1000 + (new_num & 0xFFFF))
#define CHAN_NUM(num) 			((num / 1000))
#define PURE_NUM(num) 			(num % 1000)
#define DEF_INTERPOL_SIZE		20
#define MAX_TRACKS_NUBMER		500
#define POINTS_IN_FRAME			20
#define FRAME_SIZE			1400

#define SEC_SRC_DEBUG			1

using namespace slib::algorithms;
using namespace slib::proto;

#define STRE(str) (str + strlen(str))
static char *dumpSD(GScanData &sd)
{
	static char b[512];

	sprintf(b, "Tsk:%d", sd.Task);
	sprintf(STRE(b), " Ft:%d", sd.Features);
	sprintf(STRE(b), " T:[%04lu.%04lu]", sd.LocTime.tv_sec % 1000,
						sd.LocTime.tv_nsec / 100000);
	sprintf(STRE(b), "[BLH:%.2f/%.2f/%.2f]", RAD_TO_DEG(sd.SrcPosGeo.B),
							RAD_TO_DEG(sd.SrcPosGeo.L),
							sd.SrcPosGeo.H);
	sprintf(STRE(b), " Az:%.2f", RAD_TO_DEG(sd.Azimuth));
	sprintf(STRE(b), " El:%.2f", RAD_TO_DEG(sd.Elevation));;
	sprintf(STRE(b), " NVP:%d", sd.NVarP);
	sprintf(STRE(b), " NV:%d", sd.NVar);
	for (unsigned i = 0; i< sd.NVar; i++) {
		sprintf(STRE(b), " (ID:%d", i);
		sprintf(STRE(b), " ORC:%d", sd.Var[i].ORC);
		sprintf(STRE(b), " M:%d", sd.Var[i].Marker);
		sprintf(STRE(b), " Vr:%.2f", sd.Var[i].Vr);
		sprintf(STRE(b), " D:%.2f", sd.Var[i].Distance);
		sprintf(STRE(b), " SN:%.2f", sd.Var[i].SN);
		sprintf(STRE(b), " X:%.2f Y:%.2f Z:%.2f)", sd.Var[i].PosDec.X,
							sd.Var[i].PosDec.Y,
							sd.Var[i].PosDec.Z);
	}
	return b;
}

#ifdef EXTRA_LOG
char* gtrpnt2str_n(struct GTrackPoint& _dest, int flags)
{
	static char str[800]={0};

	sprintf(str, "%lu.%lu\t%d\t%d\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%3.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f",
			_dest.LocTime.tv_sec,
			_dest.LocTime.tv_nsec,
			_dest.Id,
			_dest.ModId,
			RAD_TO_DEG(_dest.PosSph.Azimuth),
			RAD_TO_DEG(_dest.PosSph.Elevation),
			_dest.PosSph.Distance,
			RAD_TO_DEG(_dest.PosSph_f.Azimuth),
			RAD_TO_DEG(_dest.PosSph_f.Elevation),
			_dest.PosSph_f.Distance,
			_dest.Pos.X,
			_dest.Pos.Y,
			_dest.Pos.Z,
			_dest.Pos_f.X,
			_dest.Pos_f.Y,
			_dest.Pos_f.Z
			);

	return str;
}
#endif

static proto::msgTrackPoint &trPnt2msgTrPnt(struct GTrackPoint &_pnt)
{
	static struct proto::msgTrackPoint _mp;
	struct timepascal tpas;
	memset(&_mp, 0x00, sizeof(_mp));
	tspec2tpascal(_pnt.LocTime, tpas);

	_mp.ObjType        =  _pnt.ObjType;
	_mp.MsgFeature     =  _pnt.Feature;
	_mp.ObjSrcNumber   =  _pnt.Id;
	_mp.ObjModNumber   =  _pnt.ModId;
	_mp.LocTime.tv_sec =  tpas.tp_usec;
	_mp.Nationality    =  _pnt.ORC;
	_mp.Vr             =  0;
	_mp.CoordsDec      = _pnt.Pos;
	_mp.V              = _pnt.V;
	_mp.CoordRMS       = _pnt.CrdRMS;
	_mp.VelRMS         = _pnt.VelRMS;

	_mp.MsgType =  MSG_GT_TP_NP/* MSG_GT_TP_XYZ*/;

	return _mp;
}

SecDataSource::SecDataSource(int self_id, GDataSource* consumer, void *pr_data):
		GDataSource(self_id, DSRC_SECONDARY, consumer, pr_data)
#ifdef EXTRA_LOG
				,Log1(slib::log::SLog::Instance().CreateSession("",0,NULL))
#endif
{
	char str[128];
	InitGood = false;
	SelfThread = 0;
	SelfSock = -1;
	CSRPFd = -1;
	for (uint a=0; a < MAX_TRACKS_NUBMER; CurTrackNum.push_back(++a));
	InitGood = true;
	matrix.Init(MAX_TRACKS_NUBMER, MAX_TRACKS_NUBMER);
	OutBuffer = new DBuffer(500, 500);
	MsgBuffer = new DBuffer(500, 500);
#ifdef EXTRA_LOG
	sprintf(str,"log/sources/src_%d/src_%d_tracks.log", SelfId, SelfId);
	slib::log::SLog::Instance().AliasSession(Log1, str);
	Log.SetName("SRC");
#endif
}

SecDataSource::SecDataSource(int self_id, config::Config* _conf, GDataSource* consumer,
		void* pr_data): GDataSource(self_id, DSRC_SECONDARY, consumer, pr_data)
#ifdef EXTRA_LOG
				,Log1(slib::log::SLog::Instance().CreateSession("",0,NULL))
#endif
{
	char str[128];
	InitGood = false;
	SelfThread = 0;
	SelfSock = -1;
	CSRPFd = -1;
	for (uint a=0; a < MAX_TRACKS_NUBMER; CurTrackNum.push_back(++a));
	matrix.Init(MAX_TRACKS_NUBMER, MAX_TRACKS_NUBMER);
	if (!ParseConfig(_conf))
		InitGood = true;
	MsgBuffer = new DBuffer(500, 500);
	OutBuffer = new DBuffer(500, 500);
#ifdef EXTRA_LOG
	sprintf(str,"log/sources/src_%d/src_%d_tracks.log", SelfId, SelfId);
	slib::log::SLog::Instance().AliasSession(Log1, str);
	Log.SetName("SRC");
#endif
}

SecDataSource::~SecDataSource ()
{
	map<uint32_t,SecTrack*>::iterator m_it = Tracks.begin();
	while(m_it != Tracks.end()) {
		delete m_it->second;
		++m_it;
	}
	delete MsgBuffer;
	delete OutBuffer;
}

int SecDataSource::ParseConfig (config::Config* _conf)
{
	if (!_conf)
		return -EINVAL;

	if (_conf->GetRoot())
		if (_conf->GetRoot()->GetSectionEx("SELF")) {
			_conf->GetRoot()->GetSectionEx("SELF")->GetIntEx("ID", *((int *) &SelfId));
			LOGI(Log, "SelfID = " << (int)SelfId);
		} else {
			LOGE(Log, "Can't find SELF section");
			return -1;
		}
	return 0;
}

bool SecDataSource::IsGood (void)
{
	return InitGood;
}

void SecDataSource::DropTrackMarkers(void)
{
	for (auto m_it = Tracks.cbegin(); m_it != Tracks.cend();
		m_it->second->SetMarker(0), m_it++);
}

int SecDataSource::IDP_Preprocess(GScanData &sd)
{
	LOGD(Log, dumpSD(sd));

#ifdef MODEL_INPUT
	if (map_rtr2ltr.count(sd.Task)) {
		if (Tracks[map_rtr2ltr[sd.Task]]->Mode == TM_CAPTURE) {
			LOGD(Log, " !!! Insert task #" << (int)sd.Task <<
					" to track #" << (int)map_rtr2ltr[sd.Task]);
			Tracks[map_rtr2ltr[sd.Task]]->InsertNewPoint(sd, 0);
			return 1;
		} else {
			/* Force to TWS update */
			sd.Task += 100000;
		}
	}
#endif
	return 0;
}

int SecDataSource::CreateNew(GScanData &sd, unsigned flags)
{
	unsigned id;
	SecTrack* track;
	if (!sd.NVarP)
		return 0;

	for (unsigned i=0; i < sd.NVar; i++) {
		if (sd.Var[i].Marker)
			continue;

		if (CurTrackNum.empty())
			return -1;

		id = *CurTrackNum.begin();
		CurTrackNum.erase(CurTrackNum.begin());

		/* Just for sanity */
		if (Tracks.count(id)) {
			LOGE(Log, "Track with No="<< (int)id <<" existed");
			abort();
		}
		track = new SecTrack(id, this, IStrobe);
		track->InsertNewPoint(sd, i);
		Tracks[id] = track;
#ifdef MODEL_INPUT
		if (sd.Task > 100000)
			sd.Task -= 100000;
		map_rtr2ltr[sd.Task] = id;
#endif
		LOGI(Log, ">> Create new track with No=" << (int)id <<
				" Task = "<< (int)sd.Task << " <<");
	}

	return 0;
}

int SecDataSource::BindAll(GScanData &sd, unsigned flags)
{
	bool strobbed;
	unsigned row = 0;
	auto m_itb = Tracks.cbegin();
	auto m_ite = Tracks.cend();
	set<unsigned> gcols;
	map<unsigned, unsigned> row2track;
	struct MatrixDecision md[2];
	struct MatrixDecision *mdf = &md[0];

	if (!Tracks.size() || !sd.NVarP)
		return 0;

	matrix.ERows.clear();
	matrix.EColumns.clear();

	while(m_itb != m_ite) {
		strobbed = false;
		if (m_itb->second->GetMarker())
			goto skip;

		for (unsigned i=0; i < sd.NVar; i++) {
			if (sd.Var[i].Marker)
				continue;
			if (m_itb->second->InStrobe(sd, STR_SPHERE, i,
						&matrix.data[row][i])) {
				gcols.insert(i);
				strobbed = true;
			}
		}
		if (strobbed) {
			matrix.ERows.push_back(row);
			row2track[row] = m_itb->second->GetSelfId();
		}
skip:
		m_itb++;
		row++;
	}

	if (matrix.ERows.size()) {
		for (auto it=gcols.begin();
				it != gcols.end();
					matrix.EColumns.push_back(*it),it++);
	} else {
		return 0;
	}

	matrix.SaveRC();
	dpComputeByRows(matrix, md[0]);
	matrix.RestoreRC();
	dpComputeByColumns(matrix, md[1]);

	mdf = (md[1].Result > md[0].Result) ? &md[0] : &md[1];

	for (auto it = mdf->Conformance.cbegin();
			it != mdf->Conformance.cend(); ++it) {
		Tracks[row2track[it->first]]->InsertNewPoint(sd, it->second);
		LOGI(Log, ">> Update track with No=" << (int)row2track[it->first] <<
				" Task = " << (int)sd.Task << " <<");
	}

	return 0;
}

int SecDataSource::InsertDataPoint(GScanData &sd)
{
	int vn = -1;

	if (IDP_Preprocess(sd))
		goto end_of_bind;
	DropTrackMarkers();

	/* TIA processing */
	if (sd.Task) {
		if (Tracks.count(sd.Task)) {
			if (Tracks[sd.Task]->QuickUpdate(sd, STR_SPHERE, &vn)) {
				LOGD(Log, "TIA: Track[" << (int)sd.Task <<
						"] updated by SD[" << vn << "]");
			} else {
				LOGD(Log, "TIA: Track[" << (int)sd.Task <<
						"] not in strobe with TIA SD");
			}
		} else {
			LOGE(Log, "TIA: Track with No = " << static_cast<int>(sd.Task)
					<< " not existed");
		}
	}

	if (!sd.NVarP)
		goto end_of_bind;

	/* TWS and not processed points */

	/*   A. Try to bind points to existed tracks */
	BindAll(sd, 0);

	/*   B. Creating new tracks is points present */
	CreateNew(sd, 0);

end_of_bind:

	return 0;
}

uint32_t SecDataSource::GetNumTracks (void)
{
	return Tracks.size();
}

int SecDataSource::StartProcessing(void)
{
	pthread_attr_t attr;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	pthread_create(&SelfThread, &attr, SecDataSource::trdDataOut, this);
	pthread_create(&SelfThread, &attr, SecDataSource::trdDataProcessing, this);

	return 0;
}

int SecDataSource::StopProcessing(void)
{
	if (SelfThread) {
		pthread_cancel(SelfThread);
		SelfThread = 0;
	}

	return 0;
}

SecTrack* SecDataSource::GetTrackPtr(uint32_t _tr_id)
{
	if (!Tracks.count(_tr_id))
		return NULL;

	return Tracks[_tr_id];
}

void SecDataSource::Processing(void)
{

}

void* SecDataSource::DoWorkCicle(void* _arg)
{

	return NULL;
}

void SecDataSource::DropTrack(uint32_t tr_num, bool timed, uint32_t reason)
{
	struct NaviDataMsg nd;
	char buff[512] = {0};

	LOGI(Log, "!! Droping track with number " << (int)tr_num);

	if (!Tracks.count(tr_num)) {
		LOGW(Log, "Fake track kill No=" <<(int)tr_num);
		return;
	}

	GObjects.GNaviSystem->GetCurPosition(nd);
	Tracks[tr_num]->Finalize(nd);

	delete Tracks[tr_num];
	Tracks.erase(tr_num);
	CurTrackNum.push_back(tr_num);
#ifdef MODEL_INPUT
	for (auto a = map_rtr2ltr.cbegin(); a != map_rtr2ltr.cend(); a++) {
		if(a->second == tr_num) {
			map_rtr2ltr.erase(a);
			break;
		}
	}
#endif
	for(auto a = Tracks.cbegin(); a != Tracks.cend();
			sprintf(buff + strlen(buff), " %d", a++->first));
	LOGD(Log, "!! Left: " << (int)Tracks.size() << ":" << buff);
}

int SecDataSource::GetExtrTrackPoint( uint32_t track, struct NaviDataMsg& nd,
							struct GTrackPoint& res_tp)
{
	int res = -1;

	if (!Tracks.count(track))
		goto exit;
	res_tp = Tracks[track]->GetExtrapolated(nd);
	res = 0;
exit:

	return res;
}

int SecDataSource::SetSelfType (struct SourceType& _type)
{
	IStrobe[TM_CAPTURE].AddIntervals(_type.StrobeD[TM_CAPTURE]);
	IStrobe[TM_TWS].AddIntervals(_type.StrobeD[TM_TWS]);
	IStrobe[TM_TIA].AddIntervals(_type.StrobeD[TM_TIA]);

	return GDataSource::SetSelfType(_type);
}

int SecDataSource::SetOutEP(string &ep)
{
	if (SelfSock == -1)
		SelfSock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);

	struct sockaddr_in OutChnl;

	OutChnl.sin_addr.s_addr = inet_addr(ep.substr(0, ep.find(":")).c_str());
	OutChnl.sin_port = htons(atoi(ep.substr(ep.find(":") + 1,
					ep.length() - ep.find(":")).c_str()));
	OutChnl.sin_family = AF_INET;
	OutChnls.push_back(OutChnl);
	OutEnable = true;

	return 0;
}

int SecDataSource::InsertConnection (int fd)
{
	Lock();

	OutFdVector.insert(fd);
	LOGD(Log, "Connection inserted #" << fd);

	Unlock();

	return 0;
}

int SecDataSource::RemoveConnection (int fd)
{
	Lock();

	LOGD(Log, "Connection removed #" << fd);
	if (OutFdVector.count(fd))
		OutFdVector.erase(fd);

	Unlock();

	return 0;
}

int SecDataSource::RequestForUpdate(SecTrack *track)
{
	RequestMessage_t req = {0};

	if (CSRPFd == -1) {
		return -1;
	}

	req.id_s = track->GetSelfId();
	req.data.id_m = track->GetModId();
	track->GetTTU(&req.ttu);

	sendto(CSRPFd, &req, sizeof(req), 0
			, (struct sockaddr*)&CSRPEp
			, sizeof(struct sockaddr_in));

	//	LOGI(Log, "Request for update track #" << (int)track->SelfId);
	return 0;
}

int SecDataSource::SetCSRPChan(string &ep, unsigned type)
{
	if (type) {
		LOGE(Log, "Can't proceed CSRP connection type" << (int) type);
		return -1;
	}

	if (CSRPFd == -1)
		CSRPFd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);

	CSRPEp.sin_addr.s_addr = inet_addr(ep.substr(0, ep.find(":")).c_str());
	CSRPEp.sin_port = htons(atoi(ep.substr(ep.find(":") + 1
					, ep.length() - ep.find(":")).c_str()));
	CSRPEp.sin_family = AF_INET;

	return 0;
}

void SecDataSource::TimeEventProxy(TimeSystem::timeEvent *te)
{
	static_cast<SecDataSource*>(te->te_arg1)->PutMessage(0, MT_TIMEVENT,
								te, sizeof(*te));
}

int SecDataSource::ProcessTimeEvent(TimeSystem::timeEvent *te)
{
	SecTrack *t = (SecTrack *)te->te_arg2;

	if(t->Miss()) {
		DropTrack(t->SelfId, false);
	}

	return 0;
}

int SecDataSource::OutTrackPoint(struct GTrackPoint &tp)
{
	char out_buff[512];
	struct msgGeneral* msg = (struct msgGeneral*)out_buff;
	struct CrdVariant* crd = (struct CrdVariant*)(msg->Data);
	char* dptr = msg->Data + sizeof(*crd);
	struct msgTrackPointN *tpi = (struct msgTrackPointN *) dptr;
	struct msgFrame frame;
	struct timepascal tpas;
	size_t msg_size;

#ifdef EXTRA_LOG
	LOGD(Log1, gtrpnt2str_n(tp, 0));
#endif
	if (OutChnls.empty())
		goto gui;

	/* Send Data to UDP legacy clients */
	frame.Header.Time         = tpas.tp_usec;
	frame.Header.Enabled      = 1;
	frame.Header.Worked       = 1;
	frame.Header.WorkMode     = 0;
	frame.Header.UpdateTimes  = 0;
	frame.Header.EnabledZones = 0;

	frame.TrackPoints.push_back(trPnt2msgTrPnt(tp));
	convFrame2Data (frame, out_buff, sizeof(out_buff), msg_size);
	for(uint32_t chnl = 0; chnl < OutChnls.size(); chnl++) {
		sendto(SelfSock, out_buff, msg_size, 0,
		(struct sockaddr*)&(OutChnls[chnl]), sizeof(struct sockaddr_in));
	};

gui:
	/* Send data to GUI */
	Lock();
	if (OutFdVector.empty()) {
		Unlock();
		goto exit;
	}

	memset(out_buff, 0x00, sizeof(out_buff));

	msg->Header.MsgHType = MSG_T_FRAME;
	msg->Header.MsgSrc   = 0;
	msg->Header.MsgFlags = MSG_HAS_REF_POINT;
	tspec2tpascal(tp.LocTime, (struct timepascal&) msg->Header.LocTime);

	/* insert reference point */

	crd->CrdType = COORD_GEO;
	crd->CrdData.Geo = tp.SrcPosGeo;

	tpi->SubHeader.MsgType      =  MSG_T_TRACK_POINT;
	tpi->SubHeader.ObjType      =  tp.ObjType;
	tpi->SubHeader.MsgFlags     =  0; /*can be MSG_HAS_MOD_NUMBER:MSG_HAS_SUB_TRACKS*/
	tpi->SubHeader.MsgFeatures  =  tp.Feature;
	tpi->SubHeader.Nationality  =  tp.ORC;
	tpi->SubHeader.ObjSrcNumber =  tp.Id;
	tpi->SubHeader.ObjModNumber =  tp.ModId;
	tpi->SubHeader.Vr           =  tp.Vr;
	tpi->Height                 =  0.0;
	tpi->Coords.CrdType         =  COORD_DECART;
	tpi->Coords.CrdData.Dec     =  tp.Pos;
	tpi->V                      =  tp.V;
	tpi->RMSCrd                 =  tp.CrdRMS;
	tpi->RMSVel                 =  tp.VelRMS;
	tpi->NumUpdates             =  tp.NU;

	/* Sub tracks can be inserted here: "struct fldSubTracks"*/

	/* Calculating size and send data */

	dptr += sizeof(struct msgTrackPointN);
	msg_size = dptr - out_buff;
	msg_size += ((msg_size % 4) ? ( 4 - msg_size % 4) : 0);
	msg->Header.MsgSize = (uint8_t)(msg_size >> 2);
	for(auto i = OutFdVector.cbegin();
			i != OutFdVector.cend();
			send(*i++, out_buff, msg_size, 0));
	Unlock();

exit:
	return 0;
}

void *SecDataSource::trdDataOut(void *arg)
{
	size_t rsize;
	char buff[512];
	struct GDataMsg *msg = reinterpret_cast<struct GDataMsg *>(buff);
	SecDataSource *host = reinterpret_cast<SecDataSource *>(arg);

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

	while(true) {
		host->OutBuffer->Pop(buff, sizeof(buff), rsize, false);
		switch(msg->Type) {
		case MT_OUT_TP:
			host->OutTrackPoint(*((struct GTrackPoint *)msg->Data));
			break;
		default:
			LOGE(host->Log, "OUT: Uncknown message type: " << (int)msg->Type);
			break;
		}
	}

	return NULL;
}

void *SecDataSource::trdDataProcessing(void *arg)
{
	size_t rsize;
	char buff[512];
	struct GDataMsg *msg = reinterpret_cast<struct GDataMsg *>(buff);
	SecDataSource *host = reinterpret_cast<SecDataSource *>(arg);

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);

	while(true) {
		host->MsgBuffer->Pop(buff, sizeof(buff), rsize, false);

		switch(msg->Type) {
		case MT_SCAN_DATA:
			host->InsertDataPoint(*((struct GScanData *)msg->Data));
			break;
		case MT_TIMEVENT:
			host->ProcessTimeEvent((TimeSystem::timeEvent *)msg->Data);
			break;
		default:
			LOGE(host->Log, "IN: Uncknown message type: " << (int)msg->Type);
			break;
		}

	}
	return NULL;
}

int SecDataSource::PutMessage(unsigned src_id, MsgTypes type, void *data, size_t size)
{
	char buff[512];
	struct GDataMsg *msg = reinterpret_cast<struct GDataMsg *>(buff);

	msg->DestId = SelfId;
	msg->SrcId = src_id;
	msg->Type = type;
	memcpy(msg->Data, data, size);

	((type == MT_OUT_TP) ? OutBuffer :
	 	MsgBuffer)->Push(buff, size + sizeof(*msg), false);

	return 0;
}

/* Just for stub */
int SecDataSource::ProcessMessage(GDataMsg&)
{

	return 0;
}



