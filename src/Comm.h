#ifndef __COMMON_H
#define __COMMON_H

#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <execinfo.h>

//#define MODEL_INPUT
#define MINTERACTIVE

#if defined(MODEL_INPUT) && \
	defined(MINTERACTIVE)
#error "MODEL_INPUT and MINTERACTIVE can't be defined simultaneously"
#endif

#define EXTRA_LOG

enum MsgTypes {
	MT_NOT_VALID,
	MT_DATA_POINT,
	MT_TRACK_POINT,
	MT_SCAN_DATA,
	MT_TIMEVENT,
	MT_OUT_TP,
};

struct GDataMsg {
	uint32_t DestId;
	uint32_t SrcId;
	uint32_t Type;
	uint32_t DLength;
	char Data[];
};

static void bt(void)
{
	int j, nptrs;
#define SIZE 100
	void *buffer[100];
	char **strings;
	nptrs = backtrace(buffer, SIZE);
	printf("backtrace() returned %d addresses\n", nptrs);

/* The call backtrace_symbols_fd(buffer, nptrs, STDOUT_FILENO)
   would produce similar output to the following: */

	strings = backtrace_symbols(buffer, nptrs);
	if (strings == NULL) {
		perror("backtrace_symbols");
		exit(1);
	}
	for (j = 0; j < nptrs; j++)
		printf("%s\n", strings[j]);

	free(strings);
}

#endif
