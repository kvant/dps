#pragma once

#include "Types.h"
#define  DEF_TRACK_HISTORY_SIZE 25

using namespace slib::log;


enum PointMarkers
      {
         PM_BINDED = 0x01,
      };

class GTrack;
class GDataSource;


class GTrack
 {

 public:

 enum GetFlags
         {
           GET_ONLY_NEW    = 0x01,
           GET_DROP_UPDATE = 0x02,
         }; 
 
 enum TrackTypes
         {
          TT_SECONDARY,
          TT_THIRDARY
         };   

  protected:

 
   enum PointFlags
         {
            PF_PROCCESSED = 0x01,
         };

   enum TrackFlags
         {
            TF_COMPLETE = 0x01,
         };

  
  
   struct PointRegItem
         {
           struct GTrackPoint   Point;
                         void*  PrivateData;
          struct PointRegItem  *Next;
          struct PointRegItem  *Prev;  
         };  

   protected:    
                  int   SrcId;
                  int   SelfId;            
      enum TrackTypes   SelfType;
               
                 bool   Updated;   
             uint32_t   Flags;
             uint32_t   Marker;
             uint32_t   NumUpdates;   
         GDataSource&   Source;
      SLog::SLogSess&   Log;
      struct timespec   StartTime;
      struct timespec   OldestHistoryTime;
      struct timespec   LastHistoryTime;
 struct PointRegItem*   LastPoint;
 struct PointRegItem*   StorePtr;
 struct PointRegItem*   History;             

                        GTrack(GTrack&);
              GTrack&   operator=(GTrack&){return *this;}                     

    public:
                       GTrack           ( ); 
                       GTrack           ( int _tr_type, int _tr_num, GDataSource& _tr_src, int h_size = DEF_TRACK_HISTORY_SIZE );           
                      ~GTrack           ( ); 

               void    SetUpdated       ( bool enable );
               void    SetMarker        ( uint32_t marker );
               void    DropMarker       ( void );   

            uint32_t   GetSelfId        ( void );
            uint32_t   GetSrcId         ( void );
            uint32_t   GetMarker        ( void );
               bool    GetUpdated       ( void );
                           
         virtual int   InsertNewPoint   ( GTrackPoint* point, void* data = NULL );
         virtual int   GetLastPoint     ( GTrackPoint& point, uint32_t flags=0 );
         virtual int   GetHistory       ( vector<GTrackPoint>& history, uint32_t flags =0 );        
         virtual int   GetPointByND     (   );
 };

class GDataSource
{
  enum  SourceType
     {
         DSRC_NOTYPE,
         DSRC_SECONDARY,
         DSRC_THIRDARY,
     };

  enum LocationType
     {
         LOC_GENERIC,
         LOC_RINGED,
         LOC_SECTOR,  
     }; 
      
  enum PositionType
     {
         POS_MOBILE,
         POS_STATIC,
     };     
   
   protected:    
      typedef map<uint32_t, GTrack*>::iterator  tr_it_t;

                   uint32_t  SelfId;
                   uint32_t  Flags;
                   uint32_t  Marker;             
              set<uint32_t>  UpdatedTracks; 
                 SourceType  SelfType;
               PositionType  PosType;
 slib::log::SLog::SLogSess&  Log;
       struct geo::CoordGeo  Position;
     struct geo::CoordDec3D  Deplacement;
            struct timespec  WorkPeriod;
      map<uint32_t,GTrack*>  SelfTracks;
            
                      void   TrPoint2GTrPoint (struct proto::msgTrackPoint,struct GTrackPoint& _dest);   
   public:
                             DataSource ();
                             DataSource (int _self_id, SourceType type);
                            ~DataSource (); 
                        int  UpdateTraces  (char* new_data);       
                        int  ProcessMessage (proto::msgIntHeader* msg_header);
                        int  GetNewData (std::map<uint32_t,DPMatrixItem>& data); 
                        __inline__
                        void GetDeplacement(geo::CoordDec3D& _deplacement){_deplacement = Deplacement;}
                        int NewDataPresent(void); 

struct timespec GetWorkPeriod (void);
};
 

