#include "GObjects.h"
#include <pthread.h>
#include <string.h>
#include <netinet/sctp.h>
#include <arpa/inet.h>
#include <errno.h>
#include <limits.h>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#define  MAX_BUFFER_SIZE 512

using namespace     std;

static inline void frmRawPnt2GSD(slib::proto::msgRawPoint& _arg, struct GScanData &sd)
{
	struct timepascal  tpas;
	memset(&sd, 0x00, sizeof(sd));

	GObjects.GTimeSystem->GetCurrentTime(tpas);
	tpas.tp_usec = _arg.LocTime.tv_sec;
	tpascal2tspec(tpas, sd.LocTime);

	sd.Task			= _arg.ObjSrcNumber;
	sd.Features		= 0x00;
	sd.LocTime		= _arg.LocTime;
	sd.Azimuth		= _arg.CoordsSph.Azimuth;
	sd.Elevation		= _arg.CoordsSph.Elevation;
	sd.NVar			= 1;
	sd.NVarP		= sd.NVar;
	sd.Var[0].ORC		= _arg.Nationality;
	sd.Var[0].Vr		= _arg.Vr;
	sd.Var[0].Distance	= _arg.CoordsSph.Distance;
	sd.Var[0].SN		= _arg.SN;
}

static inline void frmTrPnt2GSD(slib::proto::msgTrackPoint& _arg, struct GScanData &sd)
{
	struct timepascal  tpas;
	struct NaviDataMsg nd;
	struct CoordSphere sphc;
	memset(&sd, 0x00, sizeof(sd));

	GObjects.GTimeSystem->GetCurrentTime(tpas);
	tpas.tp_usec = _arg.LocTime.tv_sec;
	tpascal2tspec(tpas, sd.LocTime);

	if(GObjects.GNaviSystem->GetPosOnTime(nd, sd.LocTime)) {
#if 0
		fprintf(stderr, "Can't get ND at %s:%d\n", __func__, __LINE__);
#endif
		sd.SrcPosGeo.B = DEG_TO_RAD(50.591809);
		sd.SrcPosGeo.L = DEG_TO_RAD(30.525048);
		sd.SrcPosGeo.H = 10;
	} else
		sd.SrcPosGeo = nd.Geo;

	sd.Task			= _arg.ObjSrcNumber;
#ifdef MINTERACTIVE
	sd.TaskM		= _arg.ObjModNumber;
#endif
	sd.Features		= 0x00;
	sd.Azimuth		= DEG_TO_RAD(_arg.CoordsSph.Azimuth);
	sd.Elevation		= DEG_TO_RAD(_arg.CoordsSph.Elevation);
	sd.NVar			= 1;
	sd.NVarP		= sd.NVar;
	sd.Var[0].ORC		= _arg.Nationality;
	sd.Var[0].Vr		= _arg.Vr;
	sd.Var[0].Distance	= _arg.CoordsSph.Distance;
	sphc.Azimuth = sd.Azimuth;
	sphc.Elevation = sd.Elevation;
	sphc.Distance = sd.Var[0].Distance;

	Sphere2Dec3D(sphc, sd.Var[0].PosDec);
}

static __inline__ void frmRawPnt2GDP(slib::proto::msgRawPoint& _arg, struct GDataPoint& _dp)
{
	memset(&_dp, 0x00, sizeof(_dp));
	_dp.Id        = _arg.ObjSrcNumber;
	_dp.ModId     = _arg.ObjModNumber;
	_dp.Feature   = _arg.MsgFeature;
	_dp.ObjType   = _arg.ObjType;
	_dp.ORC       = _arg.Nationality;
	_dp.LocPosSph = _arg.CoordsSph;
	_dp.Vr        = _arg.Vr;
	_dp.RMSa      = _arg.RMS_az;
	_dp.RMSe      = _arg.RMS_el;
	_dp.RMSd      = _arg.RMS_dist;
	_dp.SigNoise  = _arg.SN;
	_dp.LocTime   = _arg.LocTime;
	_dp.Type      = _arg.MsgType;
}

static __inline__ void frmTrPnt2GTP(slib::proto::msgTrackPoint& _arg, struct GTrackPoint& _tp)
{
	memset(&_tp,0x00,sizeof(_tp));
	_tp.Id        = _arg.ObjSrcNumber;
//	_tp.ModId     = _arg.ObjModNumber;
	_tp.Feature   = _arg.MsgFeature;
	_tp.ObjType   = _arg.ObjType;
	_tp.Vr        = _arg.Vr;
	_tp.Pos       = _arg.CoordsDec;
	_tp.V         = _arg.V;
	_tp.CrdRMS    = _arg.CoordRMS;
	_tp.VelRMS    = _arg.VelRMS;
	_tp.LocTime   = _arg.LocTime;
	_tp.ORC       = _arg.Nationality;
//	_tp.Type      = _arg.MsgType;

	_tp.CrdRMS.RMSxy  = sqrt(_tp.CrdRMS.RMSx*_tp.CrdRMS.RMSx + _tp.CrdRMS.RMSy*_tp.CrdRMS.RMSy);
	_tp.CrdRMS.RMSxyz = sqrt(_tp.CrdRMS.RMSx*_tp.CrdRMS.RMSx + _tp.CrdRMS.RMSy*_tp.CrdRMS.RMSy + _tp.CrdRMS.RMSz*_tp.CrdRMS.RMSz);

	_tp.VelRMS.RMSxy  = sqrt(_tp.VelRMS.RMSx*_tp.VelRMS.RMSx + _tp.VelRMS.RMSy*_tp.VelRMS.RMSy);
	_tp.VelRMS.RMSxyz = sqrt(_tp.VelRMS.RMSx*_tp.VelRMS.RMSx + _tp.VelRMS.RMSy*_tp.VelRMS.RMSy + _tp.VelRMS.RMSz*_tp.VelRMS.RMSz);

	if ( _arg.MsgType == MSG_T_A_BEARING || _arg.MsgType == MSG_T_AE_BEARING ){
	_tp.Pos.X     = _arg.CoordsSph.Distance*cos(_arg.CoordsSph.Elevation)*sin(_arg.CoordsSph.Azimuth);
	_tp.Pos.Y     = _arg.CoordsSph.Distance*cos(_arg.CoordsSph.Elevation)*cos(_arg.CoordsSph.Azimuth);
	_tp.Pos.Z     = _arg.CoordsSph.Distance*sin(_arg.CoordsSph.Elevation);   }

	_tp.SrcPosGeo  = _arg.RefPoint;
}


WEnvironment::WEnvironment(slib::config::Config* conf): RecvThread(0),
							ProcThread(0),
							Started(false),
							Config(conf),
							Log(slib::log::SLog::
							Instance().CreateSession("Envir",
								0, "log/envir.log"))
{
	InitGood = false;

	pthread_mutex_init(&SelfMutex, NULL);
	if (InitSources())
		return;

	if (ParseEp())
		return;

	InitGood = true;
}

WEnvironment::~WEnvironment()
{
	Stop();
	delete HostSource;
}

void WEnvironment::EstablisherStart(void)
{
	pthread_attr_t attr;

	EstablisherKill();
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
	pthread_create(&EstablThread,&attr,WEnvironment::trdConEstablisher,this);
	pthread_attr_destroy(&attr);

}

void WEnvironment::EstablisherKill(void)
{
	if (EstablThread) {
		pthread_cancel(EstablThread);
		EstablThread = 0;
	}
}

int WEnvironment::ParseEp(void)
{
	struct ConnRecord crec;
	char data[256];
	uint ncons;

	if (!Config) {
		LOGE(Log, "Bad config pointer!");
		return -1;
	}

	if (!Config->GetRoot()) {
		LOGE(Log, "Bad config file or root section!");
		return  -1;
	}

	FlushConnections();
	Connections.clear();

	//create TDS acceptor
	if (!Config->GetRoot()->GetSectionEx("CHANNELL_TDS")) {
		LOGW(Log, "No CHANNELL_TDS section in config file finded!");
		goto no_tds_acceptor;
	}

	memset(&crec, 0x00, sizeof(crec));

	crec.m_Server             = true;
	crec.m_Type               = CON_TDS_ACC;
	crec.m_LocAddr.sin_family = AF_INET;

	if  (Config->GetRoot()->GetSectionEx("CHANNELL_TDS")->GetStringEx("LOC_ADDR", data, 256)) {
		LOGW(Log, "No LOC_ADDR record in CHANNELL_TDS. Usind INADDR_ANY.");
		strcpy(data,"0.0.0.0");
	}

	crec.m_LocAddr.sin_addr.s_addr = inet_addr(data);

	if (Config->GetRoot()->GetSectionEx("CHANNELL_TDS")->GetStringEx("LOC_PORT", data, 256)) {
		LOGE(Log, "No LOC_PORT record in CHANNELL_TDS.");
		goto no_tds_acceptor;
	}

	crec.m_LocAddr.sin_port = atoi(data);
	crec.m_LocAddr.sin_port = htons(crec.m_LocAddr.sin_port);

	if (Config->GetRoot()->GetSectionEx("CHANNELL_TDS")->GetStringEx("MAX_CLIENTS", data, 256)) {
		LOGW(Log, "No MAX_CLIENTS record in CHANNELL_TDS. Set to 10.");
		strcpy(data, "10");
	}

	crec.m_MaxClients = atoi(data);
	crec.m_Fd = -1;
	Connections.insert(pair<ConnTypes, ConnRecord*>(CON_TDS_ACC, new ConnRecord(crec)));

no_tds_acceptor:

	uint32_t n = Config->GetRoot()->GetNumSections("CHANNELL_DATA");

	if (!n) {
		LOGE(Log, "No data-channels defined!!!");
		goto no_data_connections;
	}

	LOGI(Log, "Finded " << (int)n << " data connection sections.");

	for (uint32_t i = 0; i < n; i++) {
		memset(&crec, 0x00, sizeof(crec));
		crec.m_Server = false;
		crec.m_Chan2Src.clear();

		if (Config->GetRoot()->GetSection("CHANNELL_DATA", i)->GetEndPointEx("ENDPOINT", crec.m_DestAddr)) {
			LOGE(Log, "Not found ENDPOINT in CHANNELL_DATA. Skiping this section ");
			continue;
		}

		uint nsrc = Config->GetRoot()->GetSection("CHANNELL_DATA", i)->GetNumValues("CHANNEL");
		if (!nsrc) {
			LOGE(Log, "Not found SOURCE in CHANNELL_DATA. Skiping this section ");
			continue;
		}

		for (uint msrc = 0; msrc < nsrc; msrc++) {
			if (Config->GetRoot()->GetSection("CHANNELL_DATA", i)->GetString("CHANNEL", msrc, data, sizeof(data)))
				continue;

			char* b[] = { strchr(data, '('), strchr(data, ')') };
			if (!(b[0] && b[1]) || (b[0] > b[1]) || ((b[1] - b[0]) < 2)) {
				LOGE(Log, "Error CHANNEL field format. Use src_num(chnl_num). Record:" << data);
				continue;
			}

			*(b[0]) = '\0';
			*(b[1]) = '\0';

			uint _chnl = atoi(data);

			if ( crec.m_Chan2Src.count(_chnl) ) {
				LOGW(Log, "Channell " << (int)_chnl << " already present in system.");
				continue;
			}

			if (isdigit((b[0] + 1)[0])) {
				uint _src  = atoi(b[0] + 1);
				if (HostSource->GetId() != _src) {
					LOGW(Log, "Source " << (int)_src << " not present in system, but defined in channel config.");
					continue;
				}

				crec.m_Chan2Src[_chnl].m_NumChannels = 1;
				crec.m_Chan2Src[_chnl].m_ChanType =  CH_T_SINGLE;
				crec.m_Chan2Src[_chnl].m_SrcNum.push_back(_src);
				} else {
					LOGW(Log, "Channell parsing error");
					continue;
				}
		}

		crec.m_Type = CON_SRC_DATA;
		crec.m_Fd = -1;
		Connections.insert(pair<ConnTypes,ConnRecord*>(CON_SRC_DATA,new ConnRecord(crec)));
	}

no_data_connections:

	uint32_t n_ep;
	if (!Config->GetRoot()->GetSectionEx("OUT_VIEW"))
		goto no_out_view;

	n_ep = Config->GetRoot()->GetSectionEx("OUT_VIEW")->GetNumValues("ENDPOINT");
	LOGI(Log, "Founded " << (int)n_ep << " out UDP endpoints");

	for (uint32_t k = 0; k < n_ep; k++) {
		if (Config->GetRoot()->GetSectionEx("OUT_VIEW")->GetString("ENDPOINT", k, data, sizeof(data))) {
			LOGW(Log, "Get next endpoint error");
			continue;
		}
		LOGI(Log, "Seting out UDP view endpoint to: " << data);
		string str = string(data);
		HostSource->SetOutEP(str);
	}

no_out_view:

	if (!Config->GetRoot()->GetSectionEx("CSRP")) {
		LOGI(Log, "No CSRP channel found");
		goto no_csrp;
	}

	if(!(Config->GetRoot()->GetSectionEx("CSRP")->GetNumValues("ENDPOINT"))) {
		LOGI(Log, "No endpoints in CSRP channel found");
		goto no_csrp;
	}

	if (Config->GetRoot()->GetSectionEx("CSRP")->GetString("ENDPOINT", 0, data, sizeof(data))) {
		LOGW(Log, "CSRP: Get endpoint error");
		goto no_csrp;
	} else {
		LOGI(Log, "Seting CSRP endpoint to: " << data);
		string str = string(data);
		HostSource->SetCSRPChan(str);
	}

no_csrp:

	return 0;
}

int
WEnvironment::GetSrcType (uint _id, struct GDataSource::SourceType& st )
{
	slib::config::ConfigSection* cs_a;
	slib::config::ConfigSection* cs_b;

	if (SrcTypes.count(_id)) {
		st = SrcTypes[_id];
		return 0;
	}

	uint nt = SrcTConfig->GetRoot()->GetNumSections("SRC_TYPE");
	int t_id;
	unsigned b, d, mask, jj = 0;
	char cstr[256];
	struct GDataSource::SourceType new_st;
	enum TrackingMode mode;

	for (uint a = 0; a < nt; a++) {

		SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetIntEx("ID", t_id);
		if ( t_id != _id ) continue;

		LOGI(Log, "Source type found " << t_id << " loading");
		/* type parsing */
		new_st.Id = _id;

		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetStringEx("TITLE",
									cstr, sizeof(cstr)))
			return -1;

		new_st.TName = cstr;

		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetStringEx("STITLE",
									cstr,sizeof(cstr)))
			return -1;

		new_st.TSName = cstr;

		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetStringEx("VR_MSR",
									cstr,sizeof(cstr)))
			return -1;

		new_st.VrMsrt = strcmp(cstr, "YES") ? (strcmp(cstr, "NO") ? false : false) :
			true;

		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetStringEx("AIR",
									cstr,sizeof(cstr)))
			return -1;

		new_st.LocatingAir = strcmp(cstr, "YES") ? (strcmp(cstr, "NO") ? false : false)
			: true;

		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetStringEx("MARINE",
									cstr,sizeof(cstr)))
			return -1;

		new_st.LocatingWater = strcmp(cstr, "YES") ? (strcmp(cstr, "NO") ? false : false)
										: true;

		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetStringEx("DF",
									cstr, sizeof(cstr)))
			return -1;

		new_st.DF = strcmp(cstr, "YES") ? (strcmp(cstr, "NO") ? false : false) : true;

		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetStringEx("STATIC",
									cstr, sizeof(cstr)))
			return -1;

		new_st.Static = strcmp(cstr,"YES")?(strcmp(cstr,"NO") ? false : false) : true;

		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetIntEx("PROTO", t_id))
			return -1;

		new_st.Proto = t_id;

		/* Modes mask definition */
		new_st.ModeMask = 0;
		t_id = 0;
		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetIntEx("MODE_TIA", t_id))
			LOGW(Log, "MODE_TIA section is absent");
		new_st.ModeMask |= t_id ? BIT(TM_TIA) : 0;

		t_id = 0;
		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetIntEx("MODE_TWS", t_id))
			LOGW(Log, "MODE_TWS section is absent");
		new_st.ModeMask |= t_id ? BIT(TM_TWS) : 0;

		if (!new_st.ModeMask)
			LOGE(Log, "Mode mask is 0. Please allow one of modes.");

		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE",
				a)->GetDoubleEx("DROP_TIME_TWS", new_st.DropTime[TM_TWS]))
			return -1;

		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE",
				a)->GetDoubleEx("DROP_TIME_TIA", new_st.DropTime[TM_TIA]))
			return -1;

		if(SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetStringEx("SAMS",
									cstr, sizeof(cstr))) {
			new_st.IsSAMS = false;
		} else {
			new_st.IsSAMS = ((!strcmp(cstr, "YES") || !strcmp(cstr, "yes")) ? true
					: false);
		}

		/* Read strobe data if possible */
		LOGI(Log, "Reading strobe ");
		d = SrcTConfig->GetRoot()->GetSection("SRC_TYPE", a)->GetNumSections("STROBE");
		if (d < TM_MAX) {
			LOGE(Log, "Number of strobes inconsist:" << (int)d << "Should be"
					<< (int)TM_MAX);
			abort();
		}

		mask = 0;

		while(cs_a = SrcTConfig->GetRoot()->GetSection("SRC_TYPE",
								a)->GetSection("STROBE", jj++)) {
			cs_a->GetStringEx("TYPE", cstr, sizeof(cstr));
			LOGD(Log, " Strobe type = " << cstr);
			if (!strcmp(cstr, "CAPTURE")) {
				mode = TM_CAPTURE;
			} else if (!strcmp(cstr, "TWS")) {
				mode = TM_TWS;
			} else if (!strcmp(cstr, "TIA")) {
				mode = TM_TIA;
			} else {
				LOGE(Log, "Unknown strobe type: " << cstr);
				abort();
			}

			mask |= BIT(mode);

			/* Read here RMS & K */
			new_st.StrobeD[mode].mRMS = {0};
			new_st.StrobeD[mode].K = 0;

			cs_a->GetDoubleEx("RMSa", new_st.StrobeD[mode].mRMS.RMSa);
			cs_a->GetDoubleEx("RMSe", new_st.StrobeD[mode].mRMS.RMSe);
			cs_a->GetDoubleEx("RMSd", new_st.StrobeD[mode].mRMS.RMSd);
			cs_a->GetDoubleEx("K", new_st.StrobeD[mode].K);
			if(cs_a->GetDoubleEx("KF", new_st.StrobeD[mode].KF)) {
				new_st.StrobeD[mode].KF = 4.0;
			}

			new_st.StrobeD[mode].mRMS.RMSa =
					DEG_TO_RAD(new_st.StrobeD[mode].mRMS.RMSa);
			new_st.StrobeD[mode].mRMS.RMSe =
					DEG_TO_RAD(new_st.StrobeD[mode].mRMS.RMSe);
			new_st.StrobeD[mode].mRMS.RMSd *= 1e3;

			LOGD(Log, "  RMSaed = " << new_st.StrobeD[mode].mRMS.RMSa << " : " <<
					new_st.StrobeD[mode].mRMS.RMSe << " : " <<
					new_st.StrobeD[mode].mRMS.RMSd << " K=" <<
					new_st.StrobeD[mode].K << " KF=" <<
					new_st.StrobeD[mode].KF);

			if (!(t_id = cs_a->GetNumSections("INTERVAL"))) {
				LOGE(Log, "Strobe doesn't contains any interval");
				abort();
			}

			/*TODO: Free allocated memory in destructor */
			new_st.StrobeD[mode].data =
				new struct GDataSource::SrcStrobeData::Interval[t_id];

			new_st.StrobeD[mode].nitems = t_id;

			for (unsigned ii = 0; ii < t_id; ii++) {
				new_st.StrobeD[mode].data[ii] = {0};
				cs_a->GetSection("INTERVAL", ii)->GetDoubleEx("ACCEL",
							new_st.StrobeD[mode].data[ii].accel);
				cs_a->GetSection("INTERVAL", ii)->GetDoubleEx("DISTANCE",
							new_st.StrobeD[mode].data[ii].distance);
				cs_a->GetSection("INTERVAL", ii)->GetDoubleEx("DELTA",
							new_st.StrobeD[mode].data[ii].delta);
				cs_a->GetSection("INTERVAL", ii)->GetDoubleEx("RTIME",
							new_st.StrobeD[mode].data[ii].time);
				new_st.StrobeD[mode].data[ii].distance *= 1e3;
				new_st.StrobeD[mode].data[ii].delta *= 1e3;
				LOGD(Log, "  I:" << (int)ii <<
					" A:" << new_st.StrobeD[mode].data[ii].accel <<
					" DIST: " << new_st.StrobeD[mode].data[ii].distance <<
					" DELTA:" << new_st.StrobeD[mode].data[ii].delta <<
					" RTIME:" << new_st.StrobeD[mode].data[ii].time);
			}
		}
		if (mask ^ (BIT(TM_MAX + 1) - 1)) {
			LOGE(Log, "Not all strobe modes filled: " << (int)mask);
			abort();
		}

	/* ~type parsing */
		SrcTypes[_id] = new_st;
		st = new_st;

		return 0;
	}

	return -1;
}

int WEnvironment::InitSources(void)
{
	char cdata[256];
	int sint, tint;
	unsigned nsrc, srcid;
	struct GDataSource::SourceType src_type;

	HostSource = new SecDataSource(1, Config);

	SrcTConfig = slib::config::Config::NewConfig();
	slib::config::Config* SrcConfig = slib::config::Config::NewConfig();

	if (Config->GetRoot()->GetStringEx("CONFIGS.SRC_TYPES",cdata,sizeof(cdata)))
		return -1;
	if (SrcTConfig->LoadConfig(cdata))
		return -1;

	if (Config->GetRoot()->GetStringEx("CONFIGS.SOURCES",cdata,sizeof(cdata)))
		return -1;
	if (SrcConfig->LoadConfig(cdata))
		return -1;

	nsrc = SrcConfig->GetRoot()->GetNumSections("SOURCE");

	for (uint a = 0; a < nsrc; a++) {
		slib::config::ConfigSection* srcs = SrcConfig->GetRoot()->GetSection("SOURCE",a);
		if (!srcs)
			return -1;

		srcs->GetIntEx("ID", sint);
		if (sint != HostSource->GetId())
			continue;

		srcs->GetStringEx("ENABLED", cdata, sizeof(cdata));
		if (strcmp("YES", cdata)) {
			LOGW(Log, "WARN: Host source not enabled");
			continue;
		}

		if (srcs->GetIntEx("TYPE", tint)) {
			LOGW(Log, "Error get TYPE section from config for source " << (int)sint);
			continue;
		}

		if(GetSrcType(tint, src_type)) {
			LOGW(Log, "Error get type " << (int)tint << " for source "
				<< (int)sint);
			continue;
		}

		HostSource->SetSelfType(src_type);
		LOGI(Log,  "Host source " << (int)HostSource->GetId() << " created with type "
				<< (int)tint);
		break;
	} //~for a

	return 0;
}

void
WEnvironment::FlushConnections(void)
{
 multimap<ConnTypes,ConnRecord*>::iterator m_it = Connections.begin();

 while(m_it!=Connections.end())
 {
  if ( m_it->second->m_Fd != -1 ) {
       close(m_it->second->m_Fd);
       m_it->second->m_Fd = -1;   }

  if ( m_it->second->m_Type == CON_TDS_CLIENT || m_it->second->m_Type == CON_GUI_CLIENT ) {
     delete m_it->second;
     Connections.erase(m_it);
     m_it = Connections.begin();
     continue; }

  ++m_it;
 }
}

int
WEnvironment::Start(void)
{
	int ndelay = 1;
	pthread_attr_t attr;
	struct   sockaddr_in  addr;
	struct  sctp_initmsg  imsg;
	struct   ConnRecord*  crec;
	multimap<ConnTypes,ConnRecord*>::iterator c_it;

	if (!InitGood) {
		fprintf(stderr, "Init good ?\n");
		return -1; }

	if (Started)
		Stop();

	FlushConnections();

	if ((c_it = Connections.find(CON_TDS_ACC)) == Connections.end()) {
		LOGI(Log, "No server EPs detected");
		return -1;
	}

	crec = c_it->second;

	if ((crec->m_Fd = socket(PF_INET, SOCK_STREAM, IPPROTO_SCTP)) == -1) {
		LOGE(Log, "Creating sctp socket error!");
		goto error;
	}

	imsg.sinit_num_ostreams = 1;
	imsg.sinit_max_instreams = 1;
	setsockopt(crec->m_Fd, SOL_SCTP, SCTP_NODELAY, &ndelay, sizeof(ndelay));
	setsockopt(crec->m_Fd, SOL_SCTP, SCTP_INITMSG, &imsg, sizeof(imsg));

	if (bind(crec->m_Fd, (struct sockaddr*)&crec->m_LocAddr, sizeof(crec->m_LocAddr))) {
		LOGE(Log, "Error binding socket to address: " << inet_ntoa(crec->m_LocAddr.sin_addr)
			<< ":" << (int)ntohs(crec->m_LocAddr.sin_port) << " error:" << strerror(errno));
		close(crec->m_Fd);
		goto error;
	}

	if (listen(crec->m_Fd, crec->m_MaxClients)) {
		LOGE(Log, "Error go to listen mode");
		close(crec->m_Fd);
		goto error;
	}

	LOGE(Log, "Wait for external connect on " <<
			inet_ntoa(crec->m_LocAddr.sin_addr) << ":" <<
					(int)ntohs(crec->m_LocAddr.sin_port));

	PollSock = epoll_create(1);

	InsertConnect(crec);

	HostSource->StartProcessing();

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	pthread_create(&RecvThread, &attr, WEnvironment::trdIncomDataRcv, this);
	pthread_attr_destroy(&attr);

	EstablisherStart();

	Started = true;

	return 0;

error:
	FlushConnections();
	return -1;
}

int
WEnvironment::Stop(void)
{
	if (RecvThread){
		pthread_cancel(RecvThread);
		RecvThread = 0;
	}

	if (ProcThread) {
		pthread_cancel(ProcThread);
		ProcThread = 0;
	}

	HostSource->StopProcessing();
	Started = false;
	FlushConnections();

	return 0;
}

void*
WEnvironment::trdIncomDataRcv (void* arg)
{
	int res;
	int new_sock;
	int m_flags;
	uint32_t cnt;
	char rcv_buff[MAX_BUFFER_SIZE];
	char sd_buff[MAX_BUFFER_SIZE];
	socklen_t addr_len = sizeof(struct sockaddr_in);
	struct proto::msgIntHeader* msgHeader  = (struct proto::msgIntHeader*)rcv_buff;
	std::vector<struct proto::msgFrame> frame;
	struct sockaddr_in addr;
	struct epoll_event eevent  =  {(EPOLLIN | EPOLLERR | EPOLLHUP),
			 			{ 0 }
		 				};
	struct sctp_event_subscribe ev_subs = {1,0,0,0,0,0,0,0};
	struct epoll_event erevent;
	struct GTrackPoint  tr_p;
	struct GDataPoint dt_p;
	struct GScanData *sd = (struct GScanData *)sd_buff;
	struct ConnRecord *crec;
	struct ConnRecord new_crec;
	struct sockaddr_in sender_addr;
	struct sctp_sndrcvinfo sctp_info;
	class WEnvironment *host = static_cast<WEnvironment*>(arg);

	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL);

 //prepare epoll socket
	while(1) {
		epoll_wait(host->PollSock,&erevent,1,-1);
		if (!(crec=host->GetConnByFd(erevent.data.fd))) {
			LOGE(host->Log, "Cannot find connection reference to epoll socket!!!");
//			exit(1);
			continue;
		}

		switch (crec->m_Type) {
		case CON_TDS_ACC:
		case CON_GUI_ACC:
			memset(&new_crec, 0x00, sizeof(new_crec));
			new_crec.m_Type = CON_TDS_CLIENT;
			LOGI(host->Log, "Client event emited ");

			if((new_crec.m_Fd = accept(crec->m_Fd, (struct sockaddr*) &new_crec.m_DestAddr, &addr_len)) == -1) {
				LOGE(host->Log, "Error accept connection on socket" << (int)crec->m_Fd <<
						" -- " << (int)new_crec.m_Fd);
				break;
			}

			if(setsockopt(new_crec.m_Fd, SOL_SCTP, SCTP_EVENTS, &ev_subs, sizeof(ev_subs))) {
				LOGE(host->Log, "Error setsockopt on" << (int)new_crec.m_Fd);
				close(new_crec.m_Fd);
				break;
			}

			LOGI(host->Log, "Client connected from " << inet_ntoa(new_crec.m_DestAddr.sin_addr) <<
						":" << (int)ntohs(new_crec.m_DestAddr.sin_port) << "...[OK]");

			host->InsertConnect(new ConnRecord(new_crec));
			break;

		case CON_SRC_DATA:
			m_flags = 0;
			if ((res = sctp_recvmsg(crec->m_Fd, rcv_buff, sizeof(rcv_buff), (struct sockaddr*)&sender_addr,&addr_len, &sctp_info,&m_flags)) <= 0) {
				host->RemoveConnect(crec);
				break;
			}
#if 0
			if (!crec->m_Chan2Src.count(sctp_info.sinfo_stream)) {
				host->Log << error << "No source number binded to channel number " << (int)sctp_info.sinfo_stream << endr;
				break;
			}

			if (crec->m_Chan2Src[sctp_info.sinfo_stream].m_ChanType != CH_T_SINGLE) {
				host->Log << error << "Incorrect chanel type" << (int)sctp_info.sinfo_stream << endr;
				break;
			}
			frame.front().Header.SrcId = crec->m_Chan2Src[sctp_info.sinfo_stream].m_SrcNum.front();
#endif
			convData2Frame(rcv_buff, frame);

			for (uint32_t frm_cnt = 0; frm_cnt < frame.size(); frm_cnt++) {
			/* single points -> scan data */
				cnt = frame[frm_cnt].RawPoints.size();
				for (uint32_t i=0; i < cnt; i++) {
					frmRawPnt2GSD(frame[frm_cnt].RawPoints[i], *sd);
					host->HostSource->PutMessage(0, MT_SCAN_DATA, sd,
							sizeof(*sd) + sizeof(struct GScanVar) * sd->NVar);
				}

			/* track points -> scan data */
				cnt = frame[frm_cnt].TrackPoints.size();
				for (uint32_t i=0; i < cnt; i++) {
					frmTrPnt2GSD(frame[frm_cnt].TrackPoints[i], *sd);
					host->HostSource->PutMessage(0, MT_SCAN_DATA, sd,
							sizeof(*sd) + sizeof(struct GScanVar) * sd->NVar);
				}
			}
			break;
		} //switch
	} //while

return NULL;
}

void*
WEnvironment::trdConEstablisher(void* arg)
{
	int nodelay = 1;

	class WEnvironment* host = static_cast<class WEnvironment*>(arg);
	vector<ConnRecord*> records;
	vector<ConnRecord*>::iterator r_it;
	struct sctp_status sock_status;
	struct sctp_event_subscribe events;
	struct   sctp_initmsg  imsg={0};
	socklen_t arg_len  = sizeof(sock_status);
	socklen_t arg_len1 = sizeof(nodelay);
	pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,NULL);
	pthread_setcancelstate(PTHREAD_CANCEL_ENABLE,NULL);
	multimap<ConnTypes,ConnRecord*>::iterator con_it[] = {host->Connections.begin(),host->Connections.end()};


	//get not-connected records

	pthread_mutex_lock(&host->SelfMutex);

	while(con_it[0] != con_it[1]) {

		if ((con_it[0]->second->m_Type != CON_CONSUMER_MP) && (con_it[0]->second->m_Type != CON_SRC_DATA))
			goto skip;

		if (con_it[0]->second->m_Fd != -1) {
			getsockopt(con_it[0]->second->m_Fd, SOL_SCTP,SCTP_ASSOCINFO, &sock_status, &arg_len);
			if (sock_status.sstat_state == SCTP_ESTABLISHED)
				goto skip;
			close(con_it[0]->second->m_Fd);
			con_it[0]->second->m_Fd = -1;
		}

		if ((con_it[0]->second->m_Fd = socket(PF_INET,SOCK_STREAM,IPPROTO_SCTP)) == -1) {
			LOGE(host->Log, "Error open scpt socket from connector establisher");
			goto skip;
		}

		imsg.sinit_num_ostreams = 40;
		imsg.sinit_max_instreams = 40;
		setsockopt(con_it[0]->second->m_Fd, SOL_SCTP,SCTP_INITMSG, &imsg,sizeof(imsg));

		if (setsockopt(con_it[0]->second->m_Fd, SOL_SCTP, SCTP_NODELAY, &nodelay, sizeof(nodelay))) {
			LOGE(host->Log, "Error do setsockopt syscall.");
			close(con_it[0]->second->m_Fd);
			con_it[0]->second->m_Fd = -1;
			goto skip;
		}

		if (fcntl(con_it[0]->second->m_Fd, F_SETFD, O_NONBLOCK)) {
			LOGE(host->Log, "Error do fcntl syscall.");
			close(con_it[0]->second->m_Fd);
			con_it[0]->second->m_Fd = -1;
			goto skip;
		}
		printf("Insert connectiob %s:%d\n", inet_ntoa(con_it[0]->second->m_DestAddr.sin_addr), (int)ntohs(con_it[0]->second->m_DestAddr.sin_port));
		records.push_back(con_it[0]->second);
skip:
		++con_it[0];
	}

	pthread_mutex_unlock(&host->SelfMutex);

	//try to establish connect

	while(records.size()) {
		r_it = records.begin();
		while(r_it!=records.end()) {
			if(!sctp_connectx((*r_it)->m_Fd, (struct sockaddr*)&((*r_it)->m_DestAddr), 1, NULL/*,sizeof((*r_it)->m_DestAddr)*/)) {
				memset( (void *)&events, 0, sizeof(events) );
				events.sctp_data_io_event = 1;
				setsockopt((*r_it)->m_Fd, SOL_SCTP, SCTP_EVENTS, (const void *)&events, sizeof(events));

				host->InsertConnect(*r_it);
				LOGI(host->Log, "Connection establish to " << inet_ntoa((*r_it)->m_DestAddr.sin_addr) << ":"
						<< (int)ntohs((*r_it)->m_DestAddr.sin_port) << "...[OK]");

				records.erase(r_it);
				r_it = records.begin();
				continue;
			}
			LOGW(host->Log, "Connection establish to " << inet_ntoa((*r_it)->m_DestAddr.sin_addr) <<
					":" << (int)ntohs((*r_it)->m_DestAddr.sin_port) << "...[ERR]");
			++r_it;
		}
		sleep(1);
	}

	host->EstablThread = 0;
	return NULL;
}

int WEnvironment::InsertConnect(ConnRecord* record)
{
	struct epoll_event event = {(EPOLLIN | EPOLLERR | EPOLLHUP), {0} };
	event.data.fd = record->m_Fd;

	pthread_mutex_lock(&SelfMutex);

	LOGI(Log, "Insert into EPOLL new socket %d\n" << record->m_Fd);
	epoll_ctl(PollSock, EPOLL_CTL_ADD, record->m_Fd, &event);

	switch(record->m_Type) {
	case CON_GUI_CLIENT:
	case CON_TDS_CLIENT:
		HostSource->InsertConnection(record->m_Fd);
		break;
	default:
		break;
	}

	if (!GetConnByFd(record->m_Fd))
		Connections.insert(pair<ConnTypes, ConnRecord*>(record->m_Type, record));

	pthread_mutex_unlock(&SelfMutex);
	return 0;
}

int WEnvironment::RemoveConnect(ConnRecord* record)
{
	pthread_mutex_lock(&SelfMutex);

	epoll_ctl(PollSock,EPOLL_CTL_DEL,record->m_Fd,NULL);

	switch(record->m_Type) {
	case CON_SRC_DATA:
		close(record->m_Fd);
		record->m_Fd = -1;
		EstablisherStart();
		break;

	case CON_GUI_CLIENT:
	case CON_TDS_CLIENT: {
		HostSource->RemoveConnection(record->m_Fd);
		multimap<ConnTypes,ConnRecord*>::iterator m_it = Connections.find(record->m_Type);
		while(m_it != Connections.end()) {
			if(m_it->second->m_Type != record->m_Type)
				break;
			if(m_it->second->m_Fd == record->m_Fd)
				break;
			++m_it;
		}
		close(record->m_Fd);
		if (m_it->second->m_Fd == record->m_Fd)
			Connections.erase(m_it);
		delete record;
		}
		break;

	default:
		break;
	}

	pthread_mutex_unlock(&SelfMutex);
	return 0;
}

