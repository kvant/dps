#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <SLib.h>
#include <fcntl.h>
#include <setjmp.h>
#include <string.h>
#include <unistd.h>
#include "IDPS.h"

using namespace slib::timesupport;
using namespace slib::algorithms;
using namespace slib::proto;
using namespace slib::geo;

static void sigHandler(int a)
{
	exit(0);
};

int main(int argc, char* argv[])
{
	int sig;
	uint ll = 9;
	sigset_t set;
	sigfillset(&set);
//	signal(SIGINT,sigHandler);
//	signal(SIGTERM,sigHandler);
//	sigaddset(&set,SIGINT);
//	sigaddset(&set,SIGTERM);

	for (uint a = 1; a < argc; a++) {
		if (!strcmp(argv[a],"-l")) {
			if (((a + 1)!=argc) && isdigit(argv[a + 1][0])) {
				ll = atoi(argv[a + 1]);
				ll = (ll > 9) ? 9 : ll;
			} else {
				printf("WARNING: Bad command line format\n");
			}
		} else if (!strcmp(argv[a],"-d")) {
			/* ll = 0; */
			break; }
	}

	slib::log::SLog::Instance().SetLogLevel(ll);
	IDPS idps(argc, argv);
	if(idps.Start()) {
		printf("Start error\n");
		exit(1);
	}
	while(1)
		sleep(3600);
//	sigwait(&set, &sig);
	exit(0);
}
