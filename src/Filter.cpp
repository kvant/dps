#include "Filter.h"

using namespace slib::timesupport;

Filter::Filter()
{
	history = new struct IData[HISTORY_LEN];

	for (unsigned i = 1; i < HISTORY_LEN; i++) {
		history[i].next = &history[(i + 1) % HISTORY_LEN];
		history[i].prev = &history[(i - 1)];
	}
	history[0].next = &history[1];
	history[0].prev = &history[HISTORY_LEN-1];

	current = history;
	nsteps = HISTORY_LEN;
	steps = 0;

	MP[TM_TIA].Azimuth = DEG_TO_RAD(MCP_A_TIA);
	MP[TM_TIA].Elevation = DEG_TO_RAD(MCP_E_TIA);
	MP[TM_TIA].Distance = MCP_D_TIA;
	MP[TM_TWS].Azimuth = DEG_TO_RAD(MCP_A_TWS);
	MP[TM_TWS].Elevation = DEG_TO_RAD(MCP_E_TWS);
	MP[TM_TWS].Distance = MCP_D_TWS;
	K[TM_TIA] = K[TM_TWS] = K[TM_CAPTURE] = DEFAULT_K;
}

Filter::~Filter()
{
	delete[] history;
}

bool Filter::isInited(void)
{
	return inited;
}

unsigned Filter::getNumSteps(void)
{
	return steps;
}

struct timespec &Filter::getWorkTime(struct timespec &i)
{
	return i;
}

void Filter::Reset(void)
{
	steps = 0;
	current = history;
}

unsigned Filter::CheckManeuver(CoordSphere &A, CoordSphere &B, int mode)
{
	unsigned M = ((fabs(A.Distance - B.Distance) > MP[mode].Distance * K[mode]) << 0) |
		     ((fabs(A.Azimuth - B.Azimuth) > MP[mode].Azimuth * K[mode]) << 1) |
		     ((fabs(A.Elevation - B.Elevation) > MP[mode].Elevation * K[mode]) << 2);
	return M;
}

int Filter::DoKalman(unsigned mode)
{
	CMatrix_t matrix;
	CoordSphere ECrdSph;
	unsigned ret = 0;
	CoordDec3D ECrd = {
		current->prev->poutput.PosDec.X +
				current->prev->poutput.V.Vx * current->dtime,
		current->prev->poutput.PosDec.Y +
				current->prev->poutput.V.Vy * current->dtime,
		current->prev->poutput.PosDec.Z +
				current->prev->poutput.V.Vz * current->dtime,
	};
	double PE[] = {
		current->prev->P[0] + 2.0 * current->prev->P[1] * current->dtime +
				current->prev->P[2] * current->dtime * current->dtime,
		current->prev->P[1] + current->prev->P[2] * current->dtime,
		current->prev->P[2]
	};
	double Fs = 1.0 / (PE[0] + 1.0);
	double C1 = GC1(steps);
	double C2 = GC2(steps);
	double K1, K2;

	/* Extrapolation with take account of source movement */
	CalcCMatrix(current->prev->SrcPosGeo,
			current->SrcPosGeo, matrix);

	ApplyMatrix(ECrd, matrix);

	/* Check for maneuver */
	Dec3D2Sphere(ECrd, ECrdSph);

	if ((steps > STEPS_BEFORE_MC) && (mode != TM_CAPTURE) &&
			(ret = CheckManeuver(ECrdSph, current->pinput.PosSph, mode))) {
#if 0
		current->poutput = current->pinput;
		CalcV3D(current->prev->pinput.PosDec,
			current->pinput.PosDec,
			current->dtime,
			current->poutput.V);
		steps = 0;
		return ret;
#else
		steps = STEPS_JUMP_AT_M;
		C1 = GC1(steps);
		C2 = GC2(steps);
#endif
	}

	current->P[0] = PE[0] * Fs;
	current->P[1] = PE[1] * Fs;
	current->P[2] = PE[2] - PE[1] * PE[1] * Fs;

	K1 = current->P[0];
	K2 = current->P[1];
	if (K1 < C1) {
		K1 = C1;
		K2 = C2 / current->dtime;
	}

	current->poutput.PosDec.X = ECrd.X + K1 * (current->pinput.PosDec.X - ECrd.X);
	current->poutput.V.Vx = current->prev->poutput.V.Vx +
				K2 * (current->pinput.PosDec.X - ECrd.X);

	current->poutput.PosDec.Y = ECrd.Y + K1 * (current->pinput.PosDec.Y - ECrd.Y);
	current->poutput.V.Vy = current->prev->poutput.V.Vy +
				K2 * (current->pinput.PosDec.Y - ECrd.Y);

	current->poutput.PosDec.Z = ECrd.Z + K1 * (current->pinput.PosDec.Z - ECrd.Z);
	current->poutput.V.Vz = current->prev->poutput.V.Vz +
				K2 * (current->pinput.PosDec.Z - ECrd.Z);

	current->poutput.V.Vabs = sqrt(current->poutput.V.Vx * current->poutput.V.Vx +
			               current->poutput.V.Vy * current->poutput.V.Vy +
				       current->poutput.V.Vz * current->poutput.V.Vz);

	Dec3D2Sphere(current->poutput.PosDec, current->poutput.PosSph);

	return ret;
}

struct GTrackPoint &Filter::processPoint(struct GTrackPoint &arg, enum TrackingMode mode)
{
	current->LocTime = arg.LocTime;
	current->dtime = TSPEC2DBL((arg.LocTime - current->prev->LocTime));
	current->pinput.PosSph = arg.PosSph;
	current->pinput.PosDec = arg.Pos;
	current->SrcPosGeo = arg.SrcPosGeo;

	if (steps > 1) {
		arg.Maneuver = DoKalman(mode);
	} else if (steps) {
			current->poutput = current->pinput;
			CalcV3D(current->prev->pinput.PosDec,
				current->pinput.PosDec,
				current->dtime,
				current->poutput.V);

			current->P[0] = 1.0;
			current->P[1] = 1.0 / current->dtime;
			current->P[2] = 2.0 / (current->dtime * current->dtime);
		} else
			current->poutput = current->pinput;

	arg.PosSph_f = arg.PosSph;
	arg.Pos_f = arg.Pos;
	arg.V_f = arg.V;

	arg.PosSph = current->poutput.PosSph;
	arg.Pos = current->poutput.PosDec;
	arg.V = current->poutput.V;

	if (steps < nsteps)
		steps++;

	current = current->next;

	return arg;
}

void Filter::Tune(struct GDataSource::SourceType &st)
{
	MP[TM_TIA].Azimuth = st.StrobeD[TM_TIA].mRMS.RMSa;
	MP[TM_TIA].Elevation = st.StrobeD[TM_TIA].mRMS.RMSe;
	MP[TM_TIA].Distance = st.StrobeD[TM_TIA].mRMS.RMSd;
	MP[TM_TWS].Azimuth = st.StrobeD[TM_TWS].mRMS.RMSa;
	MP[TM_TWS].Elevation = st.StrobeD[TM_TWS].mRMS.RMSe;
	MP[TM_TWS].Distance = st.StrobeD[TM_TWS].mRMS.RMSd;
	K[TM_TIA] = st.StrobeD[TM_TIA].KF;
	K[TM_TWS] = st.StrobeD[TM_TWS].KF;
}


