#include "string.h"
#include <GTrack.h>
#include <GTypes.h>

#define DMP_SRC_BLH 0x01
#define DMP_SRC_XYZ 0x02
#define DMP_TRG_XYZ 0x04
#define DMP_V_XYZ   0x08
#define DMP_VR      0x10
#define DMP_COURSE  0x20
#define DMP_RMS_XYZ 0x40
#define DMP_RMS_V   0x80
#define DMP_ALL     0xFF

using namespace slib::timesupport;

static
char* gtrpnt2str(struct GTrackPoint& _dest, int flags)
{
 static char str[512]={0};
 if (!flags) return str;
 sprintf(str,"T(%lu:%lu);",_dest.LocTime.tv_sec,_dest.LocTime.tv_nsec);
 if (flags&DMP_SRC_BLH) sprintf((str+strlen(str))," sgc(%g,%g,%g);",_dest.SrcPosGeo.B,_dest.SrcPosGeo.L,_dest.SrcPosGeo.H);
 if (flags&DMP_SRC_XYZ) sprintf((str+strlen(str))," sdc(%g,%g,%g);",_dest.SrcPosDec.X,_dest.SrcPosDec.Y,_dest.SrcPosDec.Z);
 if (flags&DMP_TRG_XYZ) sprintf((str+strlen(str))," tdc(%g,%g,%g);",_dest.Pos.X,_dest.Pos.Y,_dest.Pos.Z);
 if (flags&DMP_V_XYZ) sprintf((str+strlen(str))," tvc(%g,%g,%g):%g;",_dest.V.Vx,_dest.V.Vy,_dest.V.Vz,_dest.V.Vabs);
 if (flags&DMP_VR) sprintf((str+strlen(str))," tvr(%g);",_dest.Vr);
 if (flags&DMP_COURSE) sprintf((str+strlen(str))," tc(%g);",_dest.Course);
 if (flags&DMP_RMS_XYZ) sprintf((str+strlen(str))," rc(%g,%g,%g);",_dest.CrdRMS.RMSx,_dest.CrdRMS.RMSy,_dest.CrdRMS.RMSz);
 if (flags&DMP_RMS_V) sprintf((str+strlen(str))," rv(%g,%g,%g);",_dest.VelRMS.RMSx,_dest.VelRMS.RMSy,_dest.VelRMS.RMSz);
 return str;
}

//============================================================================================

GTrack::GTrack ():Log(SLog::Instance().CreateSession("",0,NULL))
{
 Type         = -1;
 SrcId        = -1;
 TrackId      = -1;
 NumUpdates   =  0;
 HistorySize  =  0;
 Flags        =  0;
 Marker       =  0;
 Updated      =  0;
 LastNewPoint =  NULL;
 StorePtr     =  NULL;
 History      =  NULL;
 fprintf(stderr, "4444444444444444");
 memset(&LastUpdateTime,0x00,sizeof(LastUpdateTime));
 memset(&NextUpdateTime,0x00,sizeof(NextUpdateTime));
}

//============================================================================================

GTrack::GTrack (int type, int src_id, int tr_num, int h_size):Log(SLog::Instance().CreateSession("",0,""))
{
 Type         = type;
 SrcId        = src_id;
 TrackId      = tr_num;
 NumUpdates   = 0;
 HistorySize  = h_size;
 Flags        = 0;
 Marker       = 0;
 Updated      = 0;
 memset(&LastUpdateTime,0x00,sizeof(LastUpdateTime));
 memset(&NextUpdateTime,0x00,sizeof(NextUpdateTime));
 History      = (PointRegItem*) calloc(h_size,sizeof(PointRegItem));
 StorePtr     = History;
 LastNewPoint = NULL;
 for(int i=1;i<h_size-1;i++)
   {
    History[i].Next = &History[i+1];
    History[i].Prev = &History[i-1];
   }
 History[0].Next = &History[1];
 History[0].Prev = &History[h_size-1];
 History[h_size-1].Next = &History[0];
 History[h_size-1].Prev = &History[h_size-2];
}

//============================================================================================

GTrack::~GTrack()
{
 SLog::Instance().CloseSession(Log);
 if (History) free(History);
}

//============================================================================================

int
GTrack::InsertNewPoint (GTrackPoint* point, void* data)
{
        StorePtr->Point = *point;
  StorePtr->PrivateData =  data;
           LastNewPoint =  StorePtr;
         LastUpdateTime =  point->LocTime;
               StorePtr =  StorePtr->Next;
                         ++NumUpdates;
                Updated = 1;
 Log<<info<<gtrpnt2str(*point,(DMP_SRC_BLH|DMP_TRG_XYZ|DMP_V_XYZ|DMP_RMS_XYZ|DMP_RMS_V))<<endr;
 return 0;
}

//============================================================================================

int
GTrack::GetNewData(GTrackPoint& point, uint32_t flags)
{
 if (flags&GET_ONLY_NEW)
  if (!Updated) return -1;

 point = LastNewPoint->Point;

 if (flags&GET_DROP_UPDATE) Updated = 0;
 return 0;
}

//============================================================================================

int
GTrack::GetNewDataBinded(GTrackPoint& point, struct timespec& _btime, struct slib::geo::CoordDec3D& _bsrccrd)
{
 if (GetNewData(point,0)) return -1;
 struct timespec dtime = _btime - point.LocTime;
 double dtimed = TSPEC2DBL(dtime);
 double k = (10.0+dtimed)/10.0;

 point.CrdRMS.RMSx*=k;
 point.CrdRMS.RMSy*=k;
 point.CrdRMS.RMSz*=k;

 point.LocTime = _btime;
// point.SrcPos  = _bsrccrd;
 point.Marker |= PM_BINDED;
 return 0;
}

//============================================================================================




