#include "GProcessing.h"
#include "string.h"

GDataSource::GDataSource (int _self_id, SourceProcType type, GDataSource* _consumer, void* _pr_data):Log(slib::log::SLog::Instance().CreateSession("",0,NULL)),\
                                                                                                     Flags(0),Marker(0)
{
	char str[255];

	SelfId = _self_id;
	Updated = false;
	PrivData = _pr_data;
	Consumer = _consumer;
	SelfProcType = DSRC_NOTYPE;
	PosType = POS_STATIC;

	Deplacement.X = 0;
	Deplacement.Y = 0;
	Deplacement.Z = 0;

	NumChannels   = 1;

	Mode.WPeriod.tv_sec = 10;
	Mode.WPeriod.tv_nsec = 0;

	pthread_mutex_init(&SelfMutex,NULL);

	sprintf(str,"log/sources/src_%d/src_%d_main.log",SelfId,SelfId);
	slib::log::SLog::Instance().AliasSession(Log,str);
	Log.SetName("SRC");
	Log<<info<<"Source "<<(int)SelfId<<" created."<<endr;
}

GDataSource::~GDataSource ()
{
	pthread_mutex_destroy(&SelfMutex);
	slib::log::SLog::Instance().CloseSession(Log);
}

void GDataSource::GetDeplacement (CoordDec3D& _deplacement)
{
	_deplacement = Deplacement;
}

bool GDataSource::GetUpdated (void)
{
	return Updated;
}

void GDataSource::GetMode (struct SourceMode& _mode)
{
	_mode = Mode;
}

void *GDataSource::GetPData (void)
{
	return PrivData;
}

void GDataSource::SetUpdated (bool _enable)
{
	Updated = _enable;
}

int GDataSource::InsertDataPoint (GDataPoint& _point)
{
	char msg_buff[1024];
	struct GDataMsg *msg = (GDataMsg*) msg_buff;

	DataPoints.push_back(_point);

	if (DataPoints.size() > 100)
		DataPoints.erase(DataPoints.begin());

	if (!Consumer)
		return 0;

	msg->DestId  = Consumer->GetId();
	msg->SrcId   = SelfId;
	msg->Type    = MT_DATA_POINT;
	msg->DLength = sizeof(msg) + sizeof(GDataPoint);
	memcpy(msg->Data, &_point, sizeof(_point));
	Consumer->ProcessMessage(*msg);
	return 0;
}

int GDataSource::InsertDataPoint(GScanData &_point)
{
	return 0;
}

uint32_t GDataSource::GetId (void) const
{
 return SelfId;
}

int GDataSource::SetSelfType (struct SourceType& _type)
{
	SelfType = _type;
}

struct GDataSource::SourceType& GDataSource::GetSelfType ( void )
{
	return SelfType;
}

