#include <IDPS.h>
#include <GObjects.h>
#include <cstring>
#include <stdint.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

const char *const gLockFilePath = "/var/tmp/idps.pid";

struct GlobalObjects GObjects;

using namespace slib::services;
using namespace slib::timesupport;

static struct {
	struct Navigation::NaviInit  m_NaviInitData;
	struct TimeSystem::TSInitData_t  m_TSInitData;
} CmdLineParams;

class IDPSP {
	friend class IDPS;

protected:
	slib::log::SLog::SLogSess&  Log;
	bool		Daemon;
	config::Config*	Config;
	int		Daemonize(void);
	int		ParseArg(int argc, char *argv[]);
	int		Init(void);
	int		Start(void);
	int		Shutdown(int reason=0);

public:
			IDPSP(int argc, char* argv[]);
			~IDPSP();
};

IDPSP::IDPSP(int argc, char *argv[]):	Daemon(false),
					Config(config::Config::NewConfig()),
					Log(slib::log::SLog::
					Instance().CreateSession("", 0, NULL))
{
	slib::log::SLog::Instance().AliasSession(Log,"log/idps.log");
	Log.SetName("IDPS");
	Log<<info<< "IDPS created" <<endr;
	ParseArg(argc,argv);
}

IDPSP::~IDPSP()
{
	delete GObjects.GWEnvironment;
	delete GObjects.GNaviSystem;
	delete GObjects.GTimeSystem;
}

int IDPSP::Daemonize (void)
{
	int fd, res;
	char buff[64];
	pid_t pid;
	pid_t sid;

	/* create lock file */
	if ((fd = open(gLockFilePath,  O_WRONLY | O_CREAT | O_EXCL, S_IRWXU)) < 0) {
		perror("Can't create lock file.");
		exit(fd);
	}


	if ((pid = fork()) == -1) {
		cerr<<"Error hop to daemon mode" << endl;
		exit(1);
	}

	if (pid)
		exit(0);

	if ((sid = setsid()) == -1)
		exit(1);

	sprintf(buff, "%s %d\n", "idps", getpid());
	res = write(fd, buff, strlen(buff) + 1);
	unlink(gLockFilePath);
	
	if(freopen( "/dev/null", "r", stdin ));
	if(freopen( "/dev/null", "w", stdout));
	if(freopen( "/dev/null", "w", stderr));
	
	return 0;
}

int IDPSP::ParseArg(int argc, char* argv[])
{
	double t;
	char conf_file[256] = "./idps.conf";
	char conf_str[256];
	char opt;

	while ((opt = getopt(argc,argv,"l:c:d")) != -1) {
		switch (opt) {
		case 'd':
			Daemon = 1;
			break;
		case 'l':
			if (!(isdigit(optarg[0]) && (strlen(optarg) == 1))) {
				fprintf(stderr, "Log level must be in daipasone [0..9]"
					"(0 - no log, 9 - max log)\n");
				exit(1);
			}
			SLog::Instance().SetLogLevel(atoi(optarg));
			break;
		case 'c':
			strcpy(conf_file, optarg);
			break;
		case '?':
			if (optopt == 'c' )
				fprintf (stderr, "Option -c needed a argument \n");
			else if (isprint(optopt))
				fprintf (stderr, "Uncknown option: %c \n",optopt);
			else
				fprintf (stderr, "Uncknown symbol: 0x%x \n",optopt);
			break;
		default:
			exit(1);
	}
}

	if (Daemon)
		Daemonize();

	if (Config->LoadConfig(conf_file)) {
		LOGE(Log, "Error read config file");
		exit(1);
	}

	memset(&CmdLineParams, 0x00, sizeof(CmdLineParams));

	/* prepare timesystem params */

	if (!Config->GetRoot()->GetSectionEx("TIME_SYSTEM")) {
		LOGE(Log, "No TIME_SYSTEM section in config file. Bye!");
		exit(1);
	}

	if (Config->GetRoot()->GetStringEx("TIME_SYSTEM.METHOD",
					conf_str, sizeof(conf_str))) {
		LOGE(Log, "No METHOD in TIME_SYSTEM section in config file. Bye!");
		exit(1);
	}

	if (!strcmp(conf_str, "NETWORK"))
		CmdLineParams.m_TSInitData.m_Mode = TimeSystem::TSM_NET_LISTENER;
	else if(!strcmp(conf_str, "PCI"))
		CmdLineParams.m_TSInitData.m_Mode = TimeSystem::TSM_PCI_ACCESS;
	else {
		LOGE(Log, "Error time access method = "
			<< conf_str << " !!");
		exit(1);
	}

	if (CmdLineParams.m_TSInitData.m_Mode == TimeSystem::TSM_NET_LISTENER) {
		if (Config->GetRoot()->GetStringEx("TIME_SYSTEM.LISTEN_PORT",
					conf_str, sizeof(conf_str))) {
		LOGE(Log, "No LISTEN_PORT in TIME_SYSTEM section in config file. Bye!");
		exit(1);
	}
	CmdLineParams.m_TSInitData.m_Opts.Net.ListenPort = atoi(conf_str);}
	CmdLineParams.m_TSInitData.m_EventsEnable = true;

	if (!Config->GetRoot()->GetSectionEx("NAVIGATION")){
		Log << error<< "No NAVIGATION section in config file. Bye!"<<endr;
		exit(1);
	}

	if (Config->GetRoot()->GetStringEx("NAVIGATION.METHOD",conf_str,sizeof(conf_str))) {
		Log<< error <<"No METHOD in NAVIGATION section."<<endr;
		exit(1);
	}

	if (!strcmp(conf_str,"LOCAL"))
		CmdLineParams.m_NaviInitData.m_Mode = Navigation::NM_LOCAL;
	else if (!strcmp(conf_str,"REMOTE"))
		CmdLineParams.m_NaviInitData.m_Mode = Navigation::NM_REMOTE;
	else if (!strcmp(conf_str,"STREAM"))
		CmdLineParams.m_NaviInitData.m_Mode = Navigation::NM_STREAM;
	else {
		Log << error <<"Error navigation access method = "<<conf_str<<" !!"<<endr;
		exit(1);
	}

	if (Config->GetRoot()->GetStringEx("NAVIGATION.PATH",conf_str,sizeof(conf_str))) {
		Log << error <<"No PATH in NAVIGATION section in config file. Bye!"<< endr;
		exit(1);
	}

	if (CmdLineParams.m_NaviInitData.m_Mode == Navigation::NM_LOCAL)
		strcpy(CmdLineParams.m_NaviInitData.m_UnixPath,conf_str);
	else {
		char* colon = strchr(conf_str,':');
		if (!colon) {
			Log<<error<<"Bad endpoint format:"<<conf_str<<endr;
			exit(1);
		}
		CmdLineParams.m_NaviInitData.m_NetPath.sin_family = AF_INET;
		CmdLineParams.m_NaviInitData.m_NetPath.sin_port   = atoi((colon+1));
		CmdLineParams.m_NaviInitData.m_NetPath.sin_port   = htons(CmdLineParams.m_NaviInitData.m_NetPath.sin_port);
		*colon = '\0';
		CmdLineParams.m_NaviInitData.m_NetPath.sin_addr.s_addr = inet_addr(conf_str);

		if (CmdLineParams.m_NaviInitData.m_Mode == Navigation::NM_REMOTE) {
			if (Config->GetRoot()->GetStringEx("NAVIGATION.HEARTBEAT_PERIOD",
						conf_str,sizeof(conf_str))) {
				Log<<error<<"No HEARTBEAT_PERIOD in NAVIGATION section."<<endr;
				exit(1);
			}
			CmdLineParams.m_NaviInitData.m_HeartBeatDelay = atoi(conf_str);
		}
	}

	if (Config->GetRoot()->GetDoubleEx("NAVIGATION.STORE_PERIOD",t)) {
		Log<<error<<"No STORE_PERIOD in NAVIGATION section."<<endr;
		exit(1);
	}

	DBL2TSPEC(t,CmdLineParams.m_NaviInitData.m_StorePeriod);

	if(Config->GetRoot()->GetDoubleEx("NAVIGATION.STORE_INTERVAL",t)) {
		Log<<error<<"No STORE_INTERVAL in NAVIGATION section."<<endr;
		exit(1);
	}

	DBL2TSPEC(t,CmdLineParams.m_NaviInitData.m_StoreInterval);

	// log configure

	if (!Config->GetRoot()->GetSectionEx("LOG"))
		goto no_log_config;

	if (!Config->GetRoot()->GetStringEx("LOG.ENABLE_INFO",conf_str,sizeof conf_str)) {
		if (!strcmp(conf_str,"NO"))
			slib::log::SLog::Instance().SetLogFlags((slib::log::SLog::Instance().GetLogFlags() & (~slib::log::SLog::LF_ENABLE_INFO)));
		else
			slib::log::SLog::Instance().SetLogFlags((slib::log::SLog::Instance().GetLogFlags() | slib::log::SLog::LF_ENABLE_INFO));
	}

	if (!Config->GetRoot()->GetStringEx("LOG.ENABLE_DEBUG",conf_str,sizeof conf_str)) {
		if (!strcmp(conf_str,"NO"))
			slib::log::SLog::Instance().SetLogFlags((slib::log::SLog::Instance().GetLogFlags() & (~slib::log::SLog::LF_ENABLE_DBG)));
		else
			slib::log::SLog::Instance().SetLogFlags((slib::log::SLog::Instance().GetLogFlags() | slib::log::SLog::LF_ENABLE_DBG));
	}

	if (!Config->GetRoot()->GetStringEx("LOG.ENABLE_WARNING",conf_str,sizeof conf_str)) {
		if (!strcmp(conf_str,"NO"))
			slib::log::SLog::Instance().SetLogFlags((slib::log::SLog::Instance().GetLogFlags() & (~slib::log::SLog::LF_ENABLE_WARN)));
		else
			slib::log::SLog::Instance().SetLogFlags((slib::log::SLog::Instance().GetLogFlags() | slib::log::SLog::LF_ENABLE_WARN));
	}

	if (!Config->GetRoot()->GetStringEx("LOG.ENABLE_ERROR",conf_str,sizeof conf_str)) {
		if (!strcmp(conf_str,"NO"))
			slib::log::SLog::Instance().SetLogFlags((slib::log::SLog::Instance().GetLogFlags() & (~slib::log::SLog::LF_ENABLE_ERR)));
		else
			slib::log::SLog::Instance().SetLogFlags((slib::log::SLog::Instance().GetLogFlags() | slib::log::SLog::LF_ENABLE_ERR));}

	no_log_config:

	return 0;
}

int IDPSP::Init (void)
{
	LOGI(Log, "Wait for time system start.");
	GObjects.GTimeSystem = TimeSystem::NewTimeSystem(CmdLineParams.m_TSInitData);
	GObjects.GTimeSystem->Wait4Ready();
	LOGI(Log, "Time system started.");

	CmdLineParams.m_NaviInitData.m_TSystem = GObjects.GTimeSystem;

	GObjects.GNaviSystem = Navigation::NewNavigationClient(CmdLineParams.m_NaviInitData);
	LOGI(Log, "Navigation client created ");

	GObjects.GWEnvironment = new WEnvironment(Config);
	LOGI(Log, "Environment created ");

	return 0;
}

int IDPSP::Start (void)
{
	if(Init()) {
		fprintf(stderr, "IDPS: Init error \n");
		return -1;
	}

	if(GObjects.GWEnvironment->Start()) {
		fprintf(stderr, "IDPS: Env init error \n");
		return -1;
	}

	return 0;
}

int IDPSP::Shutdown (int reason)
{
	return 0;
}

IDPS::IDPS (int argc, char* argv[]):d_ptr(new IDPSP(argc,argv))
{

}

IDPS::~IDPS ()
{
	delete d_ptr;
}

int IDPS::SystemInit (void)
{
	return d_ptr->Init();
}

int IDPS::Start (void)
{
	return d_ptr->Start();
}

int IDPS::Shutdown (void)
{
	return d_ptr->Shutdown();
}
