#pragma once

#include <Comm.h>
#include <SLib.h>
#include <pthread.h>


#define  DEF_TRACK_HISTORY_SIZE 25

using namespace slib::log;
using namespace slib::geo;

/* Tracking modes */
enum TrackingMode {
	TM_CAPTURE,
	TM_TWS,
	TM_TIA,
	TM_MAX = TM_TIA,
};

enum PointMarkers
      {
         PM_BINDED    = BIT(0),
         PM_PROCESSED = BIT(1),
      };

enum PointFlags
      {
        PF_NOT_COMPLETE = BIT(0)
      };

enum DangerLevel {
	DANGER_LEVEL_3,
	DANGER_LEVEL_2,
	DANGER_LEVEL_1
};

/*enum PointType
      {
        PT_GENERIC,
        PT_NEXT_LOC,
        PT_FIRST_LOC,
        PT_EXTRAPOLATED,
        PT_INTERPOLATED,
        PT_DROP,
        PT_A_BEARING,
        PT_AE_BEARING,
        PT_NOT_COMPLETE
      };*/

struct GScanVar {
	uint8_t		ORC;
	unsigned	Marker; //processing marker
	double		Vr;
	double		Distance;
	double		SN;
	CoordDec3D	PosDec;
};

struct GScanData {
	unsigned	Task;
#ifdef MINTERACTIVE
	unsigned	TaskM;
#endif
	unsigned	Features;
	struct timespec	LocTime;
	double		Azimuth;
	double		Elevation;
	CoordGeo	SrcPosGeo;
	uint8_t		NVarP;
	uint8_t		NVar;
	GScanVar	Var[];
};

void inline GSDMarkSV(GScanData& sd, unsigned sv, unsigned marker) {

	if (!sd.NVarP || sv >= sd.NVar) {
		fprintf(stderr, "!sd.NVarP(%u) || sv(%u) >= sd.NVar(%u). Line:%u\n",
				sd.NVarP, sv, sd.NVar, __LINE__);
		return;
	}
	sd.NVarP--;
	sd.Var[sv].Marker = marker;
}

struct GDataPoint {
	uint32_t		Id;
	uint32_t		ModId;
	uint32_t		Feature;
	uint32_t		ORC;
	uint32_t		Marker;
	uint32_t		ObjType;
	uint32_t		Type;
	uint32_t		Flags;
	struct timespec		LocTime;
	CoordDec3D		LocPosDec;
	CoordSphere		LocPosSph;
	CoordGeo		SrcPosGeo;
	CoordDec3D		SrcPosDec;
	double			RMSa;
	double			RMSe;
	double			RMSd;
	double			Vr;
	double			SigNoise;
};

struct GTrackPoint
{
	uint32_t		Id;
	uint32_t		ModId; /* for test reason */
	uint32_t		Feature;
	uint32_t		ObjType;
	uint32_t		ORC;
	uint32_t		NU;
	TrackingMode		Mode;
	bool			Approaching;
	DangerLevel		Danger;
	unsigned		Maneuver;
	struct timespec		LocTime;
	double			Course;
	double			TCourse;
	double			Parameter;
	double			Vr;
	double			ATime;
	double			H;
	CoordGeo		SrcPosGeo;
	CoordSphere		PosSph;
	CoordSphere		PosSph_f;
	CoordDec3D		Pos;
	CoordDec3D		Pos_f;
	Velocity		V;
	Velocity		V_f;
	RMS	  		CrdRMS;
	RMS	 		VelRMS;
};

/**
  *=============================================================================
  *=============================================================================
  **/

class GDataSource
{
public:
  enum  SourceProcType
     {
         DSRC_NOTYPE,
         DSRC_SECONDARY,
         DSRC_THIRDARY,
     };

  enum LocationType
     {
         LOC_GENERIC,
         LOC_RINGED,
         LOC_SECTOR
     };

  enum PositionType
     {
         POS_MOBILE,
         POS_STATIC
     };

  struct SourceMode
     {
       struct timespec WPeriod;
     };

struct SrcStrobeData {
		double  K;
		double  KF;
		RMSsph  mRMS;
	unsigned	nitems;
	struct	Interval {
		double	distance;
		double	delta;
		double	time;
		double	accel;
	}		*data;
};

struct SourceType {
	uint		Id;
	uint		Proto;
	string		TName;
	string		TSName;
	bool		LocatingAir;
	bool		LocatingWater;
	bool		VrMsrt;
	bool		DF;
	bool		Static;
	bool		IsSAMS;
	uint		ModeMask;
	double		DropTime[TM_MAX + 1];
	uint		RRA[TM_MAX + 1];
	SrcStrobeData 	StrobeD[TM_MAX + 1];
};

   protected:

                   uint32_t   SelfId;
                   uint32_t   Flags;
                   uint32_t   Marker;
                   uint32_t   NumChannels;
                   uint32_t   SelfIOChannel;
		       bool   Updated;
                      void*   PrivData;
               GDataSource*   Consumer;
             SourceProcType   SelfProcType;
                 SourceMode   Mode;
               PositionType   PosType;
 slib::log::SLog::SLogSess&   Log;
            struct CoordGeo   Position;
          struct CoordDec3D   Deplacement;
            struct timespec   OldestTime;
            struct timespec   LastTime;
          struct SourceType   SelfType;
         vector<GDataPoint>   DataPoints;
            pthread_mutex_t   SelfMutex;

                             ~GDataSource ();
                              GDataSource     ( );
public:
                              GDataSource (int _self_id, SourceProcType type,
				      GDataSource* _consumer = NULL, void* _pr_data=NULL);

	bool GetUpdated(void);
	struct SourceType &GetSelfType (void);

	int BindAll(GScanData &sd, unsigned flags);
	int CreateNew(GScanData &sd, unsigned flags);

	virtual int SetSelfType(struct SourceType &_type);
	virtual int InsertDataPoint(GDataPoint &_point);
	virtual int InsertDataPoint(GScanData &_point);
	virtual int ProcessMessage(GDataMsg &_msg) = 0;

	uint32_t GetId(void) const;
	uint32_t GetNumChannels(void) const
	{
		return NumChannels;
	};

	void  GetDeplacement(CoordDec3D &_deplacement);
	void  GetMode(struct SourceMode &_mode);
	void *GetPData(void);
	void  SetUpdated(bool _enable);
	void  SetNumChannels(uint n)
	{
		NumChannels = n;
	};
	void  Lock(void) __attribute__((always_inline))
	{
		pthread_mutex_lock(&SelfMutex);
	};
	void  Unlock(void) __attribute__((always_inline))
	{
		pthread_mutex_unlock(&SelfMutex);
	};
};

