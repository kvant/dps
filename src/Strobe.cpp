#include "SecProcessing.h"

#define _GG	9.81

Strobe::Strobe():maxintervals(0),nintervals(0),data(NULL),min_distance(0),max_distance(0)
{
}

Strobe::Strobe(unsigned intervals):maxintervals(intervals),nintervals(0)
{
	data = new StrobeInterval[intervals];
	min_distance = 0;
	max_distance = 0;
}

Strobe::~Strobe()
{
	if (data)
		delete[] data;
}

int Strobe::AddIntervals(GDataSource::SrcStrobeData &idata)
{
	if (data)
		delete[] data;

	data = NULL;
	min_distance = 0;
	max_distance = 0;
	nintervals = 0;
	maxintervals = 0;
	mRMS = idata.mRMS;
	K = idata.K;

	if (!idata.nitems)
		return 0;

	data = new StrobeInterval[idata.nitems];
	maxintervals = idata.nitems;

	for (unsigned i = 0; i < idata.nitems; i++){
		AddInterval(idata.data[i].distance, idata.data[i].delta,
				idata.data[i].time, idata.data[i].accel);
	}

	return 0;
}

int Strobe::AddInterval(double distance, double delta, double time, double accel)
{
	unsigned i = nintervals - 1;
	if (nintervals == maxintervals) {
		fprintf(stderr, "Strobe: no more space for new interval\n");
		return nintervals;
	}

	if (!nintervals) {
		i = 0;
		goto add;
	}

	if (distance > max_distance) {
		i = nintervals;
		goto add;
	}

	while(distance < data[i].distance) {
		data[i + 1] = data[i];
		if (!i) break;
		i--;
	}

add:
	data[i].distance = distance;
	data[i].ddelta = delta;
	data[i].time = time;
	data[i].accel = accel;
	max_distance = data[nintervals].distance;
	min_distance = data[0].distance;

	return ++nintervals;
}

int Strobe::GetInterval(double distance, StrobeInterval *i)
{
	int n;
	if (!(data && nintervals)) {
		fprintf(stderr, "No data in strobe\n");
		abort();
	}
	n = Search(distance);
	if (i)
		*i = data[n];
	return n;
}

StrobeInterval &Strobe::GetInterval(double distance)
{
	if (!(data && nintervals)) {
		fprintf(stderr, "No data in strobe\n");
		abort();
	}

	return data[Search(distance)];
}

int Strobe::Calculate(CoordSphere &arg, double dt, SCMode mode, StrobeCResult &res)
{
	switch(mode) {
	case SCM_LAZY:
		res.sph.Azimuth = M2PI;
		res.sph.Elevation = M2PI;
		res.sph.Distance = GetInterval(arg.Distance).ddelta;
		break;
	case SCM_FULL:
		int i = GetInterval(arg.Distance, NULL);
		double accel = data[i].accel;
		double dtdt = dt * dt;

		res.sph.Azimuth = K * mRMS.RMSa + (2 * accel * _GG * dtdt) / arg.Distance;
		res.sph.Elevation = K * mRMS.RMSe + (2 * accel * _GG * dtdt) / arg.Distance;
		res.sph.Distance = K * mRMS.RMSd + 2 * accel * _GG * dtdt;
		break;
	}
	return 0;
}

