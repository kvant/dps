#pragma once

#include "GProcessing.h"
#include "Filter.h"
#include <vector>

#define DMP_SRC_BLH	BIT(0)
#define DMP_SRC_XYZ	BIT(1)
#define DMP_TRG_XYZ	BIT(2)
#define DMP_V_XYZ	BIT(3)
#define DMP_VR		BIT(4)
#define DMP_COURSE	BIT(5)
#define DMP_RMS_XYZ	BIT(6)
#define DMP_RMS_V	BIT(7)
#define DMP_MT		BIT(8)
#define DMP_OT		BIT(9)
#define DMP_ORC		BIT(10)
#define DMP_PARAMETER	BIT(11)
#define DMP_DANGER	BIT(12)
#define DMP_APPROACH	BIT(13)
#define DMP_H		BIT(14)
#define DMP_TCOURSE	BIT(15)
#define DMP_MODE	BIT(16)
#define DMP_ATIME	BIT(17)
#define DMP_MANEUVER	BIT(18)

#define DMP_ALL		0xFFFFFF

#define STR_DISTANCE	BIT(0)
#define STR_TIME	BIT(1)
#define STR_AZIMUTH	BIT(2)
#define STR_ELEVATION	BIT(3)
#define STR_ANGLES	(STR_AZIMUTH | STR_ELEVATION)
#define STR_SPHERE	(STR_ANGLES | STR_DISTANCE)
#define STR_Vx		BIT(4)
#define STR_Vy		BIT(5)
#define STR_Vz		BIT(6)
#define STR_Vr		BIT(7)
#define STR_ORC		BIT(8)

#define TM_CAPTURE_TH	3
/* Temporary classificator defines */

#define TC_H_LE		2000.0
#define TC_H_ME		6000.0
#define TC_V_LE		20.0
#define TC_V_ME		100.0
#define TC_V_HE		800.0
#define TC_D_LE		20000.0
#define TC_D_ME		30000.0
#define TC_D_HE		50000.0
#define TC_P_LE		2000.0

#define DANGEROUS_D_RANGE 30000
#define DANGEROUS_H_RANGE 1000
#define DANGEROUS_V1_RANGE 500
#define DANGEROUS_V2_RANGE 300
#define DANGEROUS_P_RANGE 2000

using namespace slib;
using namespace timesupport;
using namespace services;
using namespace algorithms;

class SecDataSource;

char* gtrpnt2str(struct GTrackPoint& _dest, int flags);


enum TrackTypes {
	TT_SECONDARY,
	TT_THIRDARY
};

enum TrackFlags {
	TF_COMPLETE = BIT(0),
	TF_BY_SAMS  = BIT(1),
};

enum TrackMarkers {
	TM_NONE,
	TM_IGNORE = BIT(0),
};

enum ProcFlags {
	PFL_ZERO = 0,
	PFL_EXTR_NO_RMS = BIT(0),
	PFL_STR_NO_EXTR = BIT(1),
};


/*
 * Temporary distance strobe and renewal
 * period container
 */

#define SRF_ANGLE_VALID		BIT(0)
#define SRF_DISTANCE_VALID	BIT(1)

struct StrobeInterval {
	double		distance;
	double		ddelta;
	double		time;
	double		accel;
};

struct StrobeCResult {
	unsigned	flags;
	CoordSphere	sph;
};

/*
 * class Strobe
 */

class Strobe {
public:
	enum SCMode {
		SCM_LAZY,
		SCM_FULL,
	};
private:
	unsigned	nintervals;
	unsigned	maxintervals;
	double		max_distance;
	double		min_distance;
	double		K;
	RMSsph		mRMS;
	StrobeInterval* data;

int inline Search(double dist)
{
	unsigned num;

	if (dist < min_distance)
		return 0;

	if (dist >= max_distance)
		return (nintervals - 1);

	num = 0;

	while(!(dist >= data[num].distance && dist < data[num + 1].distance))
		num++;

 return num;
}

public:
			Strobe();
			Strobe(unsigned intervals);
			~Strobe();
	int		GetInterval(double distance, StrobeInterval *i);
	StrobeInterval &GetInterval(double distance);
	int		AddInterval(double dist, double d, double t, double a);
	int		AddIntervals(GDataSource::SrcStrobeData &data);
	int		Calculate(CoordSphere &arg, double dt, SCMode mode,
					StrobeCResult &result);
};

/*
 * class SecTrack
 */

class SecTrack
{
protected:
	enum TEType {
		TE_MISS_TIA,
		TE_MISS_TWS,
	};
protected:
	int			SelfId;
	int			SelfModId;
	enum TrackTypes		SelfType;
	enum TrackingMode	Mode;
	bool			Updated;
	bool			Obsolete;
	uint32_t		Flags;
	uint32_t		Marker;
	uint32_t		NumUpdates;
	uint32_t		TEupdate;
	uint32_t		TEdrop;
	uint32_t		MissCount;
	struct timepascal	LastPasTime;
	struct timespec		StartTime;
	struct timespec		OldestTime;
	struct timespec		LastTime;
	struct timespec		HistoryPeriod;
	struct timespec		TTU;
	double			AvInterval;
	double			LEdt;
	double			UPdt;
	unsigned		MMask;
	void			*PrivData;
	GTrackPoint		LETPoint;
	StrobeCResult		LastSCResult;
	vector<GTrackPoint>	Points;
	SecDataSource		*Source;
	SLog::SLogSess&		Log;
	Filter			SFilter;
	Strobe			*TStrobe;

	SecTrack();
	SecTrack(const SecTrack&);;
	SecTrack &operator=(const SecTrack&);

	bool Miss(void);

	void SetupTE(unsigned types);
	void DropTE(unsigned types);

	void CompleteTrackData(GTrackPoint &p);
	void UpdateDangerous(GTrackPoint &p);
	void UpdateTrackingMode(GTrackPoint &p);
	void UpdateTrackingTime(GTrackPoint &p);
	void UpdateTargetType(GTrackPoint &p);
	void RequestForUpdate(void);
	void SendLast(void);

	friend class SecDataSource;

public:

	SecTrack(uint32_t id, SecDataSource *src, Strobe *str, void *pr_data = NULL);
	~SecTrack();

	enum TrackingMode inline GetTM(void) { return Mode; }

	void SetUpdated(bool enable);
	void SetMarker(uint32_t marker);
	void DropMarker(void);
	void SetTTU(void);
	void GetTTU(struct timespec *ttu);

	uint32_t GetSelfId(void);
	uint32_t GetModId(void);
	uint32_t GetSrcId(void);
	uint32_t GetMarker(void);
	bool GetUpdated(void);

	GTrackPoint &InsertNewPoint(GScanData& point, int vn);
	virtual GTrackPoint& GetLastPoint(uint32_t flags = 0);
	virtual int GetLastPoint(GTrackPoint& point, uint32_t flags = 0) ;
	virtual int GetHistory(vector<GTrackPoint>& history, uint32_t flags = 0);
	virtual int GetPointByND(struct GTrackPoint& point, struct NaviDataMsg& nd);
	virtual int IsObsolete(void);
	virtual GTrackPoint& Finalize(struct NaviDataMsg& nd, uint32_t reason = 0);
	__inline__ void GetLastTime(struct timepascal& _lt);

	static int RecalcPoint(struct GTrackPoint& _point);
	static int ExtrapolatePoint(struct GTrackPoint& _point, struct NaviDataMsg& _nd,
			unsigned flags = PFL_ZERO);

	GTrackPoint &GetExtrapolated(struct NaviDataMsg& _nd);
	bool InStrobe(struct GScanData &sd, unsigned flags, uint8_t vn, double *d = NULL);
	bool QuickUpdate(struct GScanData &sd, unsigned flags = 0, int *vn = NULL);
};

/*
 * class SecDataSource
 */

class SecDataSource:public GDataSource
{
protected:

	friend class SecTrack;
	typedef int (SecDataSource::*ins_dpoint)(GDataPoint& _point);
	typedef map<uint32_t,SecTrack*>::iterator  tr_it_t;

	int  SelfSock;
	int  CSRPFd;
	bool OutEnable;
	bool InitGood;
#ifdef EXTRA_LOG
	slib::log::SLog::SLogSess	&Log1;
#endif
	pthread_t			SelfThread;
	struct sockaddr_in		CSRPEp;
	vector<unsigned>  		CurTrackNum;
	map<unsigned, SecTrack*>	Tracks;
	map<unsigned, unsigned>		TrackTimeEvents;
	set<unsigned>			UpdatedSecTracks;
	map<unsigned, unsigned>		Chan2TrackId;
	vector<struct sockaddr_in>	OutChnls;
	set<int>			OutFdVector;
	Strobe				IStrobe[TM_MAX + 1];
	DMatrix				matrix;
	DBuffer				*MsgBuffer;
	DBuffer				*OutBuffer;
	ins_dpoint			cur_ins_dpoint;
#ifdef MODEL_INPUT
	map<unsigned, unsigned>		map_rtr2ltr;
#endif
	SecDataSource();

	virtual void Processing(void);

	int InsertDP_RRL(GDataPoint &_point);
	int InsertDP_SAMS(GDataPoint &_point);
	int ParseConfig(config::Config *_conf);
	int RequestForUpdate(SecTrack *track);
	int SearchBest(GScanData &sd, unsigned flags);
	int CreateNew(GScanData &sd, unsigned flags);
	int BindAll(GScanData &sd, unsigned flags);
	int IDP_Preprocess(GScanData &sd);
	int ProcessTimeEvent(TimeSystem::timeEvent *te);
	int OutTrackPoint(struct GTrackPoint &tp);

	void DropTrackMarkers(void);
	void DropTrack(uint32_t _tr_num, bool timed, uint32_t reason = 0);

	virtual int ProcessMessage(GDataMsg&);

	static void *DoWorkCicle(void *arg);
	static void TimeEventProxy(TimeSystem::timeEvent *te);
	static void *trdDataProcessing(void *arg);
	static void *trdDataOut(void *arg);

public:
	~SecDataSource();
	SecDataSource(int _self_id, GDataSource* _consumer = NULL, void* _pr_data = NULL);
	SecDataSource(int _self_id, config::Config* _conf, GDataSource* _consumer = NULL,
			void* _pr_data = NULL );

	SecTrack* GetTrackPtr(uint32_t _tr_num);

	bool IsGood(void);
	void Emulate (struct GTrackPoint& point, uint32_t flags);
	uint32_t GetNumTracks(void);

	int SetOutEP(string &ep);
	int SetCSRPChan(string &ep, unsigned type = 0);
	int InsertConnection(int fd);
	int RemoveConnection(int fd);
	int GetExtrTrackPoint(uint32_t track, struct NaviDataMsg& nd, struct GTrackPoint& res);
	int PutMessage(unsigned src_id, MsgTypes type, void *data, size_t size);

	virtual int SetSelfType(struct SourceType& _type);
	virtual int InsertDataPoint(GScanData &_point);
	virtual int StartProcessing(void);
	virtual int StopProcessing(void);
};

