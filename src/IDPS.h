#pragma once

class IDPSP;

class IDPS
   {
    private:
       IDPSP* d_ptr;           

    protected:    
            
         IDPS       ( );  
         
    public:
         IDPS       ( int argc, char* argv[] );
        ~IDPS       ( );    
     int SystemInit ( void );
     int Start      ( void );     
     int Shutdown   ( void );
   };
