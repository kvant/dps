#pragma once

#include "WEnvironment.h"
#include "TimeSystem.h"
#include "Navigation.h"

struct GlobalObjects
   {
          Navigation*  GNaviSystem;
          TimeSystem*  GTimeSystem;
        WEnvironment*  GWEnvironment;
   };

extern struct GlobalObjects GObjects;
