#pragma once

#include <SLib.h>
#include <map>
#include "SecProcessing.h"

using namespace slib;

class WEnvironment
{
  public:

    enum  ConnTypes
        {
          CON_NOT_VALID,
          CON_SRC_DATA,
          CON_TDS_ACC,
          CON_TDS_CLIENT,
          CON_GUI_ACC,
          CON_GUI_CLIENT,
          CON_CONSUMER_MP
        };

     enum ChannelTypes
        {
         CH_T_SINGLE,
         CH_T_MULTI,
         CH_T_CONSUMER,
         CH_T_SAMS
        };

  typedef
  struct{
          ChannelTypes   m_ChanType;
                  uint   m_NumChannels;
          vector<uint>   m_SrcNum;
        } ChanDescr_t;

   struct ConnRecord
        {
                     int   m_Fd;
                    bool   m_Server;
                uint32_t   m_MaxClients;
               ConnTypes   m_Type;
      struct sockaddr_in   m_DestAddr;
      struct sockaddr_in   m_LocAddr;
      struct ConnRecord*   m_Acceptor;
   map<uint,ChanDescr_t>   m_Chan2Src;
        };

                                 int    PollSock;
                                bool    Started;
                                bool    InitGood;
     multimap<ConnTypes,ConnRecord*>    Connections;
                           pthread_t    RecvThread;
                           pthread_t    ProcThread;
                           pthread_t    EstablThread;
                     pthread_mutex_t    SelfMutex;
                       SecDataSource*   HostSource;
                      config::Config*   Config;
                      config::Config*   SrcConfig;
                      config::Config*   SrcTConfig;
          slib::log::SLog::SLogSess&    Log;
           map<uint32_t,GDataSource*>   Sources;
   map<uint, GDataSource::SourceType>   SrcTypes;


                            int    GetSrcType        ( uint _id,struct GDataSource::SourceType& st );
                            int    InitSources       ( void );
                            int    ParseEp           ( void );
                            int    InsertConnect     ( ConnRecord* record );
                            int    RemoveConnect     ( ConnRecord* record );
                           void    EstablisherKill   ( void );
                           void    EstablisherStart  ( void );
                           void    FlushConnections  ( void );

                    static void*   trdIncomDataRcv   ( void* );
                    static void*   trdConEstablisher ( void* );

                                   WEnvironment();

                    ConnRecord*    GetConnByFd(int fd) __attribute((always_inline))
                                    {
                                      multimap<ConnTypes,ConnRecord*>::iterator m_it = Connections.begin();
                                      multimap<ConnTypes,ConnRecord*>::iterator e_it = Connections.end();
                                      while(m_it!=e_it)
                                        {
                                           if (m_it->second->m_Fd == fd) return m_it->second;
                                         ++m_it;
                                        }
                                      return NULL;
                                    };

public:
                               WEnvironment(config::Config* );
                              ~WEnvironment();

                        int    Start    (void);
                        int    Stop     (void);
};
