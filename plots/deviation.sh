#!/bin/bash

if [[ ! -a tr_$1.log.dev ]]
then
	while read line
	do
		LINE=$(echo $line | grep "_OK_")
		if [[ -z $LINE ]]; then continue; fi
		RES=$(echo $LINE | awk '{ print $6}' | awk '{print $1}' | cut -f1 -d/ | cut -f2 -d:)
		RES="$RES $(echo $LINE | awk '{ print $6}' | awk '{print $1}' | cut -f2 -d/)"
		RES="$RES $(echo $LINE | awk '{ print $7}' | awk '{print $1}' | cut -f1 -d/ | cut -f2 -d:)"
		RES="$RES $(echo $LINE | awk '{ print $7}' | awk '{print $1}' | cut -f2 -d/)"
		RES="$RES $(echo $LINE | awk '{ print $8}' | awk '{print $1}' | cut -f1 -d/ | cut -f2 -d:)"
		RES="$RES $(echo $LINE | awk '{ print $8}' | awk '{print $1}' | cut -f2 -d/)"
		echo $RES >> tr_$1.log.dev
	done < tr_$1.log
fi

echo " plot \"tr_$1.log.dev\" using 1 w lines title 'Deviation_D', \"tr_$1.log.dev\" using 2 with lines title 'Strobe_D Max'" | gnuplot -p
echo " plot \"tr_$1.log.dev\" using 3 w lines title 'Deviation_A', \"tr_$1.log.dev\" using 4 with lines title 'Strobe_A Max'" | gnuplot -p
echo " plot \"tr_$1.log.dev\" using 5 w lines title 'Deviation_E', \"tr_$1.log.dev\" using 6 with lines title 'Strobe_E Max'" | gnuplot -p

