#!/bin/bash

CMD="splot"

rm -f *.crd

for i in `ls *.log`; do
	echo $i
	cat $i | grep tdc | awk '{ print $8" "$9" "$10 }' > $i.crd
	CMD="$CMD \"$i.crd\" u 1:2:3 with lines,"
done
echo ${CMD:0: $(expr ${#CMD} - 1)}
#> plot.txt
echo ${CMD:0: $(expr ${#CMD} - 1)} | tee gp_cmd.txt | gnuplot -persist -clear -tvtwm
##gnuplot -p plot.txt
sleep 1000
